<?php

use DoctrineExtensions\NestedSet\Config;
use DoctrineExtensions\NestedSet\Manager;
use DoctrineExtensions\NestedSet\MultipleRootNode;
use Doctrine\Common\ClassLoader;

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initLocale() {
        date_default_timezone_set('Europe/Berlin');

        $locale = new Zend_Locale();
        Zend_Registry::set('Zend_Locale', $locale);
    }

    protected function _initConfig() {
        Zend_Registry::set('config', $this->getOptions());
    }

    protected function _initRouters() {
        $front = Zend_Controller_Front::getInstance();
        $config = new Zend_Config_Yaml(APPLICATION_PATH . '/configs/routes.yaml');
        $front->getRouter()->addConfig($config, 'routes');
    }

    protected function _initSession() {
        $sessionConfig = array(
            "use_only_cookies" => true,
            "name" => "platzhalter",
            "save_path" => APPLICATION_PATH . "/tmp",
            "throw_startup_exceptions" => true
        );
        Zend_Session::setOptions($sessionConfig);
    }

    protected function _initZendX() {
        $this->bootstrap('view');
        $view = $this->getResource('view');

        $view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
    }

    protected function _initPlugins() {
        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Conlabz_Controller_Plugin_LoggedIn());
    }

    protected function _initBoilerplate() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
        $view->addHelperPath(APPLICATION_PATH . '/modules/default/views/helpers', 'Inv_View_Helper');
        $view->addHelperPath(APPLICATION_PATH . '/../library/Conlabz/View/Helper', 'Conlabz_View_Helper');
        //Meta
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8');

        //CSS
        $view->headLink()
                ->appendStylesheet('/css/bootstrap.min.css')
                ->appendStylesheet('/css/bootstrap-responsive.min.css')
                ->appendStylesheet('/css/font-awesome.min.css')
                ->appendStylesheet('/css/jquery.feedBackBox.css')
                ->appendStylesheet('/css/style.css')
                ->appendStylesheet('/css/style1.css')
                ;

        //Title
        $view->headTitle('conlabz Inventarverwaltung')
                ->setSeparator(' :: ');

        //JavaScript
        $view->headScript()
                ->appendFile('/js/libs/modernizr-2.5.3-respond-1.1.0.min.js')
                ->appendFile('/js/libs/bootstrap/bootstrap.min.js')
                ->appendFile('/js/jquery/jquery.editinplace.js')
                ->appendFile('/js/jquery/jquery-combobox.js')
                ->appendFile('/js/jquery/nestedsets.js')
                ->appendFile('/js/jquery/jquery.dataTables.min.js')
                ->appendFile('/js/jquery/jquery.feedBackBox.js')
                ->appendFile('/js/plugins.js')
                ->appendFile('/js/script.js')
                ->appendFile('/js/jquery/comment.js')
                ->appendFile('/js/feedback.js');
        
        


        //jQuery
        $view->jQuery()
                ->addStylesheet('/css/jquery/start/jquery-ui-twitter-bootstrap.css')
                ->setLocalPath('/js/jquery/jquery-1.8.1.min.js')
                ->setUiLocalPath('/js/jquery/jquery-ui-1.9.2.custom.min.js');

        //jQuery enable
        $view->jQuery()->enable();

        //jQuery ui enable
        $view->jQuery()->uiEnable();
    }

    protected function _initClassLoaders() {
        require_once('Doctrine/Common/ClassLoader.php');

        $loader = new ClassLoader('Doctrine\ORM');
        $loader->register();
        $loader = new ClassLoader('Doctrine\DoctrineExtensions');
        $loader->register();
        $loader = new ClassLoader('Doctrine\Common');
        $loader->register();
        $loader = new ClassLoader('Doctrine\DBAL');
        $loader->register();
        $loader = new ClassLoader('Symfony', 'Doctrine');
        $loader->register();
    }

    protected function _initDoctrine() {
        $dbConfig = $this->getOption('doctrine');
        $db = Zend_Db::factory($dbConfig['driver'], array(
                    'host' => $dbConfig['host'],
                    'username' => $dbConfig['username'],
                    'password' => $dbConfig['password'],
                    'dbname' => $dbConfig['dbname']
        ));

        return $db;
    }

    protected function _initImageMagick() {
        $config = $this->getOption("imagemagick");
        Zend_Registry::set("convert", $config['convert']);
        Zend_Registry::set("montage", $config['montage']);
        
    }

    protected function _initEntityManager() {
        require 'Doctrine/DoctrineExtensions/NestedSet/Node.php';
        require 'Doctrine/DoctrineExtensions/NestedSet/Config.php';
        require 'Doctrine/DoctrineExtensions/NestedSet/MultipleRootNode.php';
        require 'Doctrine/DoctrineExtensions/NestedSet/Manager.php';
        require 'Doctrine/DoctrineExtensions/NestedSet/NodeWrapper.php';
        $config = new \Doctrine\ORM\Configuration();

        $cache = new \Doctrine\Common\Cache\ArrayCache();

        $config->setMetadataCacheImpl($cache);
        $config->setProxyDir(realpath(APPLICATION_PATH . '/proxies'));
        $config->setProxyNamespace('App\Proxies');

        $driverImpl = $config->newDefaultAnnotationDriver(APPLICATION_PATH . '/modules/default/models/Entity');
        $config->setMetadataDriverImpl($driverImpl);

        $config->setAutoGenerateProxyClasses(true);
        //$config->setSQLLogger(new Doctrine\DBAL\Logging\EchoSqlLogger());

        $connectionOptions = array(
            'pdo' => $this->getResource('doctrine')->getConnection()
        );

        $em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);
        $em->getConnection()->setCharset('UTF8');


        $schemaTool = new Doctrine\ORM\Tools\SchemaTool($em);
        $metadata = $em->getMetadataFactory()->getAllMetadata();

        #$schemaTool->updateSchema($metadata);


        $cfg = new Config($em, 'Entity\Category');
        $nsm = new Manager($cfg);

        Zend_Registry::set('nestedSetManager', $nsm);
        Zend_Registry::set('entityManager', $em);
    }

    protected function _initNavigation() {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();
        $config = new Zend_Config_Yaml(APPLICATION_PATH . '/configs/navigation.yaml', 'nav');

        $navigation = new Zend_Navigation($config);
        $view->navigation($navigation);
    }

}
