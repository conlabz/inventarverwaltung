<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */
abstract class Application_Model_HtmlTable_Column
{
	/**
	 *
	 * how to format the row
	 * @var string
	 */
	protected $_formatViewHelper = NULL;

	/**
	 *
	 * variable the fitler
	 * @var array
	 */
	protected $_filter = array();

	/**
	 *
	 * default attr
	 * @var array
	 */
	protected $_attr = array(
		"id" 	=> NULL,
		"class" => NULL
	);

	/**
	 *
	 * default options
	 * @var array
	 */
	protected $_options = array(
		"internalName" 	=> NULL,
		"displayName" 	=> NULL,
		"sortable"		=> false,
		"filter"		=> false,
		"link"			=> false,
		"grouping"		=> false
	);

	/**
	 *
	 * type from the culumn
	 * @var string
	 */
	protected $_columnType = NULL;

	/**
	 *
	 * @var Zend_Form_Element_Select
	 */
	protected $_grouping = NULL;

	protected function __construct($internalName, $displayName, $isSortable = false, $isGrouping = false, $isLink = false, array $attrs = array())
	{
		$this->setOption("internalName", $internalName);
		$this->setOption("displayName", $displayName);
		$this->setOption("sortable", $isSortable);
		$this->setOption("link", $isLink);

		$this->setAttrs($attrs);
		if($isGrouping)
		{
			$this->setDefaultGrouping();
		}
	}

	protected function setDefaultGrouping()
	{
		$name = "grouping".ucfirst($this->getOption("internalName"));
		$element = new Zend_Form_Element_Select($name);
		$defaults = Application_Model_System_Session::getValue("tableDefaults");
		$element->setDecorators(array('ViewHelper'));
		$entries = array(
		0 	=> " ",
		1	=> "gruppieren"
		);
		$element->addMultiOptions($entries);
		if(isset($defaults[$name]))
		{
			$element->setValue($defaults[$name]);
		}
		$this->_grouping = $element;
		$this->_options['grouping'] = true;
	}

	public function getGrouping()
	{
		return $this->_grouping;
	}

	protected function setFormatViewHelper($name)
	{
		$this->_formatViewHelper = $name;
	}

	public function getFormatViewHelper()
	{
		return $this->_formatViewHelper;
	}

	protected function setFilter($element)
	{
		if(!empty($element))
		{
			$this->_filter[] 			= $element;
			$this->_options["filter"] 	= true;
		}
	}

	public function getFilters()
	{
		if(!empty($this->_filter))
		{
			return $this->_filter;
		}
		return false;
	}

	protected function setAttrs(array $attrs)
	{
		$this->_attr = $attrs;
	}

	protected function setAttr($key, $value)
	{
		if(!empty($key) && !empty($value))
		{
			$this->_attr[$key] = $value;
		}
	}

	public function getAttrs()
	{
		return $this->_attr;
	}

	public function getAttr($key)
	{
		if(!empty($key))
		{
			return $this->_attr[$key];
		}
	}

	protected function setOption($key, $value)
	{
		if(!empty($key) && !empty($value))
		{
			$this->_options[$key] = $value;
		}
	}

	public function getOption($key)
	{
		if(!empty($key))
		{
			return $this->_options[$key];
		}
	}

	protected function setColumnType($type)
	{
		$this->_columnType = $type;
	}

	public function getColumnType()
	{
		return $this->_columnType;
	}
}