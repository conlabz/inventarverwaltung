<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */

class Application_Model_HtmlTable_ColumnDate extends Application_Model_HtmlTable_Column
{
	public function __construct($internalName, $displayName, $isSortable = false, $filter = false, $isGrouping = false, $isLink = false, array $attr = array())
	{
		parent::__construct($internalName, $displayName, $isSortable, $isGrouping, $isLink, $attr);

		if($filter)
		{
			$this->_setFilter();
		}
		
		$this->setFormatViewHelper("date");
		$this->setColumnType("date");
	}

	/**
	 *
	 * bild the filter field(s) and
	 * append this to the filter
	 */
	private function _setFilter()
	{
		$element = new ZendX_JQuery_Form_Element_DatePicker($this->getOption("internalName"));
		$defaults = Adm_Model_System_Session::getValue("tableDefaults");
		if(isset($defaults[$this->getOption("internalName")]))
		{
			$element->setValue($defaults[$this->getOption("internalName")]);
		}
		$element->setJQueryParam('dateFormat', 'dd.mm.yy');
		$element->setJQueryParam('changeYear', true);
		$element->setJQueryParam('changeMonth', true);
		$element->setJQueryParam('yearRange', '2008');
		$element->setJQueryParam('monthNamesShort', array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"));
		$element->setJQueryParam('dayNamesShort', array("Mon", "Di", "Mi", "Do", "Fri", "Sa", "So"));
		$element->setDecorators(array('ViewHelper'));
		$element->setDecorators(
		array(
               	'UiWidgetElement'
               	)
               	);
               	$this->setFilter($element);
	}
}