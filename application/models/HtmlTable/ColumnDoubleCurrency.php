<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */

class Application_Model_HtmlTable_ColumnDoubleCurrency extends Application_Model_HtmlTable_Column
{
	public function __construct($internalName, $displayName, $isSortable = false, $filter = false, $isGrouping = false, $isLink = false, array $attr = array())
	{
		parent::__construct($internalName, $displayName, $isSortable, $isGrouping, $isLink, $attr);

		if($filter)
		{
			$this->_setFilter();
		}

		$this->setFormatViewHelper("currency");
		$this->setColumnType("currency");
	}

	private function _setFilter()
	{
		for($i = 1; $i <= 2; $i++)
		{
			$name = $this->getOption("internalName").$i;
			$element = new Zend_Form_Element_Text($name);
			$defaults = Adm_Model_System_Session::getValue("tableDefaults");
			if(isset($defaults[$name]))
			{
				$element->setValue($defaults[$name]);
			}
			$element->setDecorators(array('ViewHelper'));
			$this->setFilter($element);
		}
	}
}