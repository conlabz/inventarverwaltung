<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */

class Application_Model_HtmlTable_ColumnPeriod extends Application_Model_HtmlTable_Column
{
	public function __construct($internalName, $displayName, $isSortable = false, $filter = false, $isGrouping = false, $isLink = false, array $attr = array())
	{
		parent::__construct($internalName, $displayName, $isSortable, $isGrouping, $isLink, $attr);

		if($filter)
		{
			$this->_setFilter();
		}

		
		
		if($isGrouping)
		{
			$this->setGrouping();
		}
		
		$this->setFormatViewHelper("datePeriod");

		$this->setColumnType("date");
	}

	/**
	 *
	 * bild the filter field(s) and
	 * append this to the filter
	 */
	private function _setFilter()
	{
		$elements = array();
		for($i = 1; $i <= 2; $i++)
		{
			$name = $this->getOption("internalName").$i;
			$element = new Zend_Form_Element_Text($name);
			$defaults = Adm_Model_System_Session::getValue("tableDefaults");
			if(isset($defaults[$name]))
			{
				$element->setValue($defaults[$name]);
			}
			//$element->setJQueryParam('dateFormat', 'MM yy');
			//$element->setJQueryParam('changeYear', true);
			//$element->setJQueryParam('changeMonth', true);
			//$element->setJQueryParam('yearRange', '2008');
			//$element->setJQueryParam('monthNames', array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"));
			//$element->setJQueryParam('monthNamesShort', array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"));
			//$element->setJQueryParam('dayNamesShort', array("Mon", "Di", "Mi", "Do", "Fri", "Sa", "So"));
			$element->setDecorators(array('ViewHelper'));
			//$element->setDecorators(
			//	array('UiWidgetElement')
            //);
            $this->setFilter($element);
		}
	}
	
	private function setGrouping()
	{
		$name = "grouping".ucfirst($this->getOption("internalName"));
		$element = new Zend_Form_Element_Select($name);
		$element->setDecorators(array('ViewHelper'));
		$entries = array(
			0			=> " ",
			"quarter" 	=> "Quartal",
			"half"		=> "1/2 Jahr"
		);
		$element->addMultiOptions($entries);
		$defaults = Adm_Model_System_Session::getValue("tableDefaults");
		if(isset($defaults[$name]))
		{
			$element->setValue($defaults[$name]);
			
		}
		$this->setFormatViewHelper(NULL);
		$this->_grouping = $element;
		$this->_options['grouping'] = true;
	}
}