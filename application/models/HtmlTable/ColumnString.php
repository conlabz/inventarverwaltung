<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */

class Application_Model_HtmlTable_ColumnString extends Application_Model_HtmlTable_Column
{
	public function __construct($internalName, $displayName, $isSortable = false, $filter = false, $isGrouping = false, $isLink = false, array $attr = array())
	{
		parent::__construct($internalName, $displayName, $isSortable, $isGrouping, $isLink, $attr);

		if($filter)
		{
			$this->_setFilter();
		}
		
		$this->setColumnType("string");
	}

	private function _setFilter()
	{
		$element = new Zend_Form_Element_Text($this->getOption("internalName"));
		$defaults = Application_Model_System_Session::getValue("tableDefaults");
		if(isset($defaults[$this->getOption("internalName")]))
		{
			$element->setValue($defaults[$this->getOption("internalName")]);
		}
		$element->setDecorators(array('ViewHelper'));
		$this->setFilter($element);

	}
}