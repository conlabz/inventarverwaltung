<?php
/**
 *
 *
 * @author Julian Gilles (Conlabz GmbH)
 *
 */

class Application_Model_HtmlTable_ColumnZip extends Application_Model_HtmlTable_Column
{
	public function __construct($internalName, $displayName, $isSortable = false, $filter = false, $isGrouping = false, $isLink = false, array $attr = array())
	{
		parent::__construct($internalName, $displayName, $isSortable, $isGrouping, $isLink, $attr);

		if($filter)
		{
			$this->_setFilter();
		}
		
		if($isGrouping)
		{
			$this->setGrouping();
		}
		
		$this->setColumnType("string");
	}

	private function _setFilter()
	{
		$element = new Zend_Form_Element_Text($this->getOption("internalName"));
		$defaults = Adm_Model_System_Session::getValue("tableDefaults");
		if(isset($defaults[$this->getOption("internalName")]))
		{
			$element->setValue($defaults[$this->getOption("internalName")]);
		}
		$element->setDecorators(array('ViewHelper'));
		$this->setFilter($element);

	}
	
	private function setGrouping()
	{
		$name = "grouping".ucfirst($this->getOption("internalName"));
		$element = new Zend_Form_Element_Select($name);
		$element->setDecorators(array('ViewHelper'));
		$entries = array(
			0			=> " ",
			1 	=> "erste Stelle",
			2	=> "ersten zwei Stellen",
			3	=> "ersten drei Stellen"
		);
		$element->addMultiOptions($entries);
		$defaults = Adm_Model_System_Session::getValue("tableDefaults");
		if(isset($defaults[$name]))
		{
			$element->setValue($defaults[$name]);
		}
		$this->_grouping = $element;
		$this->_options['grouping'] = true;
	}
}