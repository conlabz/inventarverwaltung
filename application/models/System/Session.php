<?php 
/**
 * 
 * session model
 * @author Julian Gilles (Conlabz GmbH)
 *
 */
class Application_Model_System_Session
{
	private static $session;
	
	/**
	 * 
	 * @param string $namespace
	 * @return Zend_Session_Namespace
	 */
	public static function getSession($namespace = "inventory")
	{
		if(empty(self::$session))
		{
			self::$session = new Zend_Session_Namespace($namespace);
			return self::$session;
		} 
		return self::$session;
	}
	
	/**
	 * 
	 * beendet die Session
	 * @param string $namespace
	 */
	public static function unsetSession($namespace = "inventory")
	{
		Zend_Session::namespaceUnset($namespace);
	}
	
	/**
	 * 
	 * wie lange die Anwendung sich an den user erinnern soll
	 */
	public static function rememberMe()
	{
		Zend_Session::rememberMe(14*24*3600);
	}
	
	/**
	 * 
	 * gib einen value aus der Session zurück
	 * @param string $value
	 * @throws Zend_Exception
	 * @return mixed
	 */
	public static function getValue($value)
	{
		if(empty($value))
		{
			throw new Zend_Exception("Es muss ein Paramenter übergeben werden!");
		}
		
		$session = self::getSession();
		
		if(isset($session->$value))
		{
			return $session->$value;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * setzt einen Wert in die Session
	 * @param string $key
	 * @param string $value
	 * @throws Zend_Exception
	 * @return string
	 */	
	public static function setValue($key, $value)
	{
		if(empty($key))
		{
			throw new Zend_Exception("Es muss ein key übergeben werden!");
		}
		
		$session = self::getSession();
		$session->$key = $value;
	}
	
	/**
	 * 
	 * erstellt die Session für den Codeigniter-Teil der Anwendung
	 * @param string $sessionName
	 */
	public static function setCodeigniterSession($sessionName = "ci")
	{
		$session = array();
		$session['session_id'] 	= self::getValue('session_id');
		$session['ip_address'] 	= self::getValue('userip');
		$session['user_agent'] 	= self::getValue('useragent');
		$session['id'] 			= self::getValue('user_id');
		$session['username']	= self::getValue('username');
		$session['email']		= self::getValue('email');
		$session['active']		= 1;
		$session['last_visit']	= self::getValue('last_visit');
		$session['created']		= self::getValue('created');
		$session['modified']	= self::getValue('last_modified');
		$roles 					= self::getValue('roles');
		foreach ($roles as $role)
		{
			if($role == "client")
			{
				$group = 4;
			}
			
			if($role == "admin")
			{
				$group = 2;
			}
			
			if($role == "office")
			{
				$group = 5;
			}
			
			if($role == "service_employee")
			{
				$group = 3;
				
			}
			if($role == "employee")
			{
				$group = 3;
			}
		}
		$session['group_id']	= $group;
		$session['group']		= self::getGroupName($group);
		$session['lastname']	= "";
		$session['fristname']	= "";
		$session['performance'] = "";
		
		$service = false;
		$employee = false;
		foreach ($roles as $role)
		{
			if($role == "service_employee")
			{
				$service = true;
			}
			
			if($role == "employee")
			{
				$employee = true;
			}
		}
		
		if(!$employee && $service)
		{
			$session['serviceMA'] = 1;
		}
		
		self::setValue($sessionName, $session);
	}

	/**
	 * 
	 * Bekommt die Gruppen ID übergeben und liefert den Namen zurück
	 * @param int $id
	 */
	private static function getGroupName($id)
	{
		switch ($id) {
			case 1:
				return 'Member';
				break;
			case 2:
				return 'Administrator';
				break;
			case 3:
				return 'Employee';
				break;
			case 4:
				return 'Vendor';
				break;
			case 5:
				return 'Office';
				break;
		}
	}
}