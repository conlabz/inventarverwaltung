<?php 
use Doctrine\Common\ClassLoader;
class Default_Bootstrap extends Zend_Application_Module_Bootstrap 
{
    public function _initTranslate()
    {   
        try {
            $locale = Zend_Registry::get('Zend_Locale');
        } catch(Exception $e) {
            $locale = new Zend_Locale();
        }
        
        $language = $locale->getLanguage();
        
        $translationFile = dirname(__FILE__) . '/language/' . $language . '.csv';
        
        if(!file_exists($translationFile)) {
            $translationFile = dirname(__FILE__) . '/language/en.csv';
        }
        
        $translate = new Zend_Translate(
            array(
                'adapter' => 'csv',
                'content' => $translationFile,
                'locale'  => $language,
                'delimiter' => ','
            )
        );

        Zend_Form::setDefaultTranslator($translate);
        Zend_Controller_Router_Route::setDefaultTranslator($translate);
        Zend_Registry::set('Zend_Translate', $translate);
        Zend_Registry::set('translationFile', $translationFile);
    }
    
    protected function _initSession() 
    {
        $sessionConfig = array(
            "use_only_cookies" 	=> true,
            "name" => "platzhalter",
            "save_path" => APPLICATION_PATH."/tmp",
            "throw_startup_exceptions" => true
        );
        Zend_Session::setOptions($sessionConfig);
    }
    
    public function _initSetCache()
    {
        $path = APPLICATION_PATH . '/tmp';
        $cache = Zend_Cache::factory(
            'Core',
            'File',
            array(),
            array('cache_dir' => $path)
        );
        Zend_Locale::setCache($cache);
        Zend_Translate::setCache($cache);
    }
    
    public function _initAutoload() 
    {
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
                        'basePath'  => dirname(__FILE__),
                        'namespace' => 'Inv',
        ));
        $resourceLoader->addResourceTypes(array(
            'form' => array(
                'path'      => 'forms/',
                'namespace' => 'Form',
            ),
            'model' => array(
                'path'      => 'models/',
                'namespace' => 'Model'
            )
        ));
    }
    
    public function _initDoctrineAutoload() 
    {
        $loader = new ClassLoader('Entity', dirname(__FILE__) . '/models');
        $loader->register();
    }
    
    public function _initView()
    {
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('pagination.phtml');
    }
    
}
