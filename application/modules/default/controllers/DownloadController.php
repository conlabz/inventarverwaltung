<?php
/**
 * Description of Download
 *
 * @author hummer
 */
class DownloadController extends Zend_Controller_Action 
{
    public function init()
    {
        $this->repository = new Inv_Model_Repository(); 
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    public function invoiceAction()
    {
        $invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id', 0));
        $file = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $invoice->getFile();

        if(!is_file($file)) {
            $this->_response->setHttpResponseCode(404);
            $this->_forward();
            return;
        }   
        
        echo readfile($file);

        $this->getResponse()
            ->setHeader('Content-type', 'application/pdf')
            ->setHeader('Content-Disposition', 'attachment;filename="'. $this->view->translate('Invoice') . '_' . $invoice->getInvoicenr() . '.pdf');
    }
}
