<?php

class EditController extends Zend_Controller_Action {

    protected $em;
    protected $repository;
    protected $output = array(
        'success' => false
    );

    public function init() {
        $this->repository = new Inv_Model_Repository();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('inventory', 'json')
                ->addActionContext('invoice', 'json')
                ->addActionContext('delete-inventory', 'json')
                ->initContext();
    }

    /**
     * Edit Inventory
     */
    public function inventoryAction() {

        $this->em = Zend_Registry::get('entityManager');

        $array = array();
        if ($this->getRequest()->isPost()) {
            $array = Application_Model_System_Session::getValue("ids");
        } elseif ($this->getRequest()->isGet()) {
            Application_Model_System_Session::setValue("ids", array());
        }




        $this->view->headTitle()->prepend('Inventar bearbeiten');
        $this->view->title = 'Inventar bearbeiten';

        $id = $this->_getParam('id', 0);
        // Find inventory to edit
        $inventory = $this->repository
                ->getEm()
                ->find('Entity\Inventory', $id);


        $this->view->id = $inventory->getId();
        $request = $this->getRequest();


        // fill form
        $formDefaults = array(
            'category' => $inventory->getCategory()->getId(),
            'label' => $inventory->getLabelId(),
            'model' => $inventory->getModel()->getId(),
            'person' => $inventory->getPerson()->getId(),
            'manufacturer' => $inventory->getManufacturer()->getId(),
            'distributor' => $inventory->getDistributor()->getId(),
            'id' => $inventory->getId(),
            'serialnr' => $inventory->getSerialnr(),
            'location' => $inventory->getLocation(),
            'price' => $inventory->getPrice(),
            'invoice' => $inventory->getInvoiceId(),
            'account' => $inventory->getAccount()->getId(),
            'invaccnr' => $inventory->getInvaccnr(),
            'recorddate' => $inventory->getRecorddate()->format('Y-m-d'),
        );

        $form = new Inv_Form_Default();
        $form->setAction($request->getPathInfo());
        $form->setDefaults($formDefaults);
        // Add delete  button
        $delete = new Zend_Form_Element_Submit('delete');
        $delete->setOptions(array(
            'label' => 'delete',
            'class' => 'btn btn-danger'
        ));
        $delete->setDecorators(array('ViewHelper'));
        $form->addElement($delete);

        // Form submitted


        if ($request->isPost()) {
            $post = $request->getPost();



            // Delete?
            if (isset($post['delete'])) {
                try {
                    $this->repository->remove($inventory);
                    $this->_helper->Success(
                            'Inventar wurde gelöscht', '/'
                    );
                } catch (Exception $e) {
                    $this->view->errors = $e->getMessage();
                }
            }

            try {
                if ($form->isValid($post)) {


// Create Inventory!
                    $invFactory = new Inv_Model_Factory_Inventory($inventory, $form->getValues());
                    $inv = $invFactory->createItem();
                    if ($array != NULL) {
                        foreach ($array as $invoiceid) {

                            $invoice = $this->repository->find('Entity\Invoice', $invoiceid);
                            $inv->addInvoices($invoice);
                        }
                    }
                    $this->repository->refreshStatus($this->_getParam('id', NULL), $form->getValues());


                    $this->repository->save($inv);

                    Application_Model_System_Session::setValue("ids", array());
                    
                    $this->_helper->Success(
                            'Inventar wurde gespeichert!', $this->view->url(array('id' => $id), 'view', true)
                    );
                } else {
                    $this->view->formErrors = $form->getMessages();
                }
            } catch (Exception $e) {
                $this->view->errors = $e->getMessage();
            }
        }



        // QR-Code Placeholder
        $view = $this->getHelper('ViewRenderer')->view;
        $view->id = $id;
        //$this->view->invoice = $invoice;
        $this->view->placeholder('qr-code')->set($view->render('index/qr-code.phtml'));

        $this->view->form = $form;

        $this->view->invoiceForm = new Inv_Form_Invoice();
        $this->renderScript('new/inventory.phtml');
    }

    public function invoiceAction() {
        $request = $this->getRequest();
        $form = new Inv_Form_Invoice();
        if (!$invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id', 0))) {
            $this->_redirect($this->view->url(array('controller' => 'new')));
        }
        $formDefaults = array(
            'invoicenr' => $invoice->getInvoicenr(),
            'invoicedate' => $invoice->getInvoicedate(),
            'invoice_id' => $invoice->getId(),
            'invoice_pdf' => $invoice->getFile()
        );
        $form->setDefaults($formDefaults);
        $form->setAction($request->getPathInfo());

        if ($request->isPost()) {
            $post = $request->getPost();
            if ($form->isValid($post)) {
                try {

                    $this->repository->saveInvoice($post);
                    $this->_helper->Success(
                            $this->view->translate('Invoice saved.'), $this->view->url(array('controller' => 'invoice', 'action' => 'view', 'id' => $invoice->getId()), 'default', true)
                    );
                } catch (Exception $e) {
                    $form->setDefaults($post);
                    $this->view->errors = $this->view->translate($e->getMessage());
                }
            } else {
                $this->view->formErrors = $form->getMessages();
            }
        }
        $this->view->invoice = $invoice;
        $this->view->form = $form;
    }

    public function deleteInvoicepdfAction() {
        $invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id', 0));
        Inv_Model_DeletingFile::delete($invoice);
        $this->output['success'] = true;
        $this->_helper->json->sendJson($this->output);
    }

    public function deleteInventoryAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                $inventory = $this->repository->find('Entity\Inventory', $request->getParam('id'));
                $this->repository->remove($inventory);
                $this->_helper->Success(
                        'Inventar wurde gelöscht', '/'
                );
            } catch (Exception $e) {
                $this->view->error = $e->getMessage();
            }
        }
    }

    public function deleteItemAction() {
        $entity = 'Entity\\' . ucfirst($this->_getParam('entity'));
        $item = $this->repository->find($entity, $this->_getParam('id'));

        $output = array();
        $output['success'] = false;
        try {
            if ($entity == 'Entity\\Invoice') {
                Inv_Model_DeletingFile::delete($item);
            }
            $this->repository->remove($item);
            $output['success'] = true;
        } catch (Exception $e) {
            $output['error'] = 'Element konnte nicht gelöscht werden!';
        }
        $this->_helper->json->sendJson($output);
    }

    public function renameItemAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $post = $this->getRequest()->getPost();

        $data = explode('_', $post['element_id']);
        $entity = 'Entity\\' . ucfirst($data[0]);
        $method = 'set' . ucfirst($data[2]);

        $newName = trim($post['update_value']);

        if (!empty($newName)) {
            $id = $data[1];
            $item = $this->repository->find($entity, $id);
            $item->{$method}($post['update_value']);
            $this->repository->save($item);
            echo $newName;
        } else {
            echo $post['original_html'];
        }
    }

    public function saveTreeAction() {
        $post = $this->_request->getPost();
        $output = array();
        $output['success'] = false;
        try {
            $this->repository->saveTree(json_decode(stripslashes($post['tree'])));
            $output['success'] = true;
            $output['msg'] = $this->view->translate('Tree saved.');
        } catch (Exception $e) {
            $output['msg'] = 'Error saving the tree: ' . $e->getMessage();
        }
        $output['select'] = $this->view->nestedSetSelect($this->repository->getCategories());
        $this->_helper->json->sendJson($output);
    }

    public function coreDataAction() {

        $nsm = Zend_Registry::get('nestedSetManager');

        $tree = $nsm->fetchTree(1);
        #$tree->addChild($parent);
        #$child = $nsm->wrapNode($child);
        #$child->delete();
        #$parent->addChild($child);

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if (!empty($post['childname'])) {
                $parent = $this->repository->find('Entity\Category', $post['parent']);

                $category = new Entity\Category();
                $category->setName($post['childname']);
                $parent = $nsm->wrapNode($parent);
                $parent->addChild($category);
            }
        }

        $this->view->headTitle()->prepend('Stammdaten bearbeiten');

        $categories = $this->repository->getCategories();
        $this->view->categories = $categories;

        $models = $this->repository->getAcData('Entity\Model', array('name'));
        $this->view->models = array_filter($models);

        $labels = $this->repository->getAcData('Entity\Label', array('name'));
        $this->view->labels = array_filter($labels);

        $persons = $this->repository->findAll('Entity\Person');
        $this->view->persons = $persons;

        $manufacturer = $this->repository->getAcData('Entity\Manufacturer', array('name'));
        $this->view->manufacturer = array_filter($manufacturer);

        $distributor = $this->repository->getAcData('Entity\Distributor', array('name'));
        $this->view->distributor = array_filter($distributor);

        $accounts = $this->repository->findAll('Entity\Account');
        $this->view->accounts = $accounts;

        $invoices = $this->repository->findAll('Entity\Invoice');
        $this->view->invoices = $invoices;
    }

}
