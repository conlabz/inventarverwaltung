<?php

class IndexController extends Zend_Controller_Action
{   
    /**
     * the inventory data model
     * @object
     */
    protected $repository;
    
    /**
     * items per page
     * @integer
     */
    protected $itemsPerPage = 15;
    
    /**
     * inventory order
     * @array
     */
    protected $order = array();
    
    /**
     * table search inputs
     * @array
     */
    protected $tableDefaults = null;
    
    /**
     * filter
     * @array
     */
    protected $filter = array();
    
    /**
     * excluded keys for filter
     * @array
     */
    protected $filterExcludeKeys = array('itemsperpage');
      
    public function init() 
    {
        // set repository
        $this->repository = new Inv_Model_Repository();
       
        // set items count per page
        $itemsPerPage = Application_Model_System_Session::getValue('itemsPerPage');
        if(is_numeric($itemsPerPage)) {
            $this->itemsPerPage = (int) $itemsPerPage;
        }
        // set table defaults
        $tableDefaults = Application_Model_System_Session::getValue('tableDefaults');
        if(!empty($tableDefaults)) {
            $this->tableDefaults = $tableDefaults; 
        }
        
        // set orderby
        $this->order = array(
            'column' => $this->_getParam('column', 'id'),
            'order'  => $this->_getParam('order', 'ASC')
        );
        
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('language', 'json')                    
                    ->initContext();
    }
    
    /**
     * sets filter 
     * @return filter array
     */
    protected function setFilter($data) 
    {
        foreach($data as $key => $value) {
            $value = trim($value);
            if(!empty($value) && !in_array($key, $this->filterExcludeKeys)) {
                if($key == "status") {
                    $value = Inv_Model_Status::statusToInt($value);
                }
                
                $this->filter[$key] = $value;
            }
        }
        
        
        return $this->filter;
    }
    
    /**
     * Display table / overview
     */
    public function indexAction() 
    {          
        // form submitted?
        if($this->getRequest()->isPost()) {
            
            $post = $this->getRequest()->getPost();
            
            if(is_numeric($post['itemsperpage'])) {
                Application_Model_System_Session::setValue('itemsPerPage', (int) $post['itemsperpage']);
                $this->itemsPerPage = (int) $post['itemsperpage'];
            } 
            
            // check what button was pressed
            switch(true) {
            case isset($post['reset']):
                Application_Model_System_Session::setValue('tableDefaults', null);
                Application_Model_System_Session::setValue('itemsPerPage', 15);
                $this->itemsPerPage = 15;
                break;
            case isset($post['refresh']):
            default:
                $filter = $this->setFilter($post);
                $tblFilter = $filter;
               if(array_key_exists('status',$tblFilter)) {
                $tblFilter['status'] = \Inv_Model_Status::intToStatus($tblFilter['status']);
               }
               
                Application_Model_System_Session::setValue("tableDefaults", $tblFilter);
                break;
            }
            $query = new Inv_Model_QueryBuilder($this->filter, $this->order);
            
        } else {
            // no post
            $query = new Inv_Model_QueryBuilder($this->tableDefaults, $this->order);
        }
        
        $paginator = new Zend_Paginator(new Conlabz_Paginator_Adapter_Doctrine(
                
            $query->buildFilteredQuery(), $query->buildCountQuery()
                
                
              
        ));
                
        $paginator->setCurrentPageNumber($this->_getParam('page'), 1);
        $paginator->setItemCountPerPage($this->itemsPerPage);
                
        $inventories = array();
        // create array for table-helper
        foreach($paginator as $key => $inv) {
            $inventories[$key]['id']['text'] = $inv->getId();
            $inventories[$key]['id']['link'] = $this->view->url(
                array('id' => $inv->getId()), 'view');
            $inventories[$key]['category']     = $inv->getCategoryName();
            $inventories[$key]['label']        = $inv->getLabelName();
            $inventories[$key]['model']        = $inv->getModelName();
            $inventories[$key]['location']     = $inv->getLocation();
            $inventories[$key]['distributor']  = $inv->getDistributorName();
            $inventories[$key]['manufacturer'] = $inv->getManufacturerName();
            $inventories[$key]['person']       = $inv->getPersonFirstname().' '.$inv->getPersonLastname();      
            $inventories[$key]['serialnr']     = $inv->getSerialnr();
            $inventories[$key]['price']        = $inv->getPrice();
            $inventories[$key]['account']      = $inv->getAccountNumber();
            $inventories[$key]['invaccnr']     = $inv->getInvaccnr();
            $inventories[$key]['status']       = $inv->getStatus(true);
        }
        
        $this->view->inventories = $inventories;
        $this->view->paginator   = $paginator;       
    }
    
    /**
     * get Inventory item by id / details view
     */
    public function viewAction() 
    {
        $id = $this->_getParam('id', 0);
        // Find inventory to edit
        $inventory = $this->repository
                        ->getEm()
                        ->find('Entity\Inventory', $id);
        $this->view->headTitle()->prepend($inventory->getId() . ' ' . $inventory->getModel()->getName());
        $this->view->inventory = $inventory;
        
                
        // QR-Code Placeholder
        $view = $this->getHelper('ViewRenderer')->view;
        $view->id = $id;
        $this->view->placeholder('qr-code')->set($view->render('index/qr-code.phtml'));
    }
    
    /**
     * export table data 
     */
    public function exportAction() 
    {
        $this->_helper->layout()->disableLayout();
        
        $invs = $this->repository->getFiltered($this->tableDefaults, $this->order);

        $this->view->inventories = $invs;
    }
    
    /**
     * get json language for js translations
     */
    public function languageAction()
    {
        $translation = new Inv_Model_Translation();
        $this->view->dictionary = $translation->getTranslations();
    }

}
