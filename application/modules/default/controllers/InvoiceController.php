<?php
class InvoiceController extends Zend_Controller_Action
{
    protected $repository;
    
    public function init()
    {
        $this->repository = new Inv_Model_Invoice();
    }
    
    public function indexAction()
    {   
        $this->view->invoices = $this->repository->getInvoices();
    }
    
    public function viewAction()
    {
        $this->view->invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id'));
        $this->view->id = $this->_getParam('id');
        
    }
}
