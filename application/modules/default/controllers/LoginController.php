<?php
class LoginController extends Zend_Controller_Action
{
    
     protected $repository;
     
    public function init()
    {
        $this->_em = Zend_Registry::get('entityManager');
        $this->_auth = Zend_Auth::getInstance();
    }
    
    /**
     * login / form
     */
    public function indexAction() 
    {
        $form = new Inv_Form_Login();
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                if (!$this->_process($form->getValues())) {                 
                    $this->_helper->FlashMessenger(
                        array('alert alert-error' => 'Falsche Zugangsdaten.')
                    );
                }
                $this->_helper->redirector('index', 'index');
            }
        }
        $this->view->form = $form;
    }
    
    
    public function registAction() {
        $form = new Inv_Form_Regist();
         $this->view->form = $form;
        
         $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                try {
               $data = $request->getPost();
               Inv_Model_Mail::sendRegist($data);
               $this->_helper->redirector('index', 'index');
          
                }
                catch(Exception $e) {
                     $this->_helper->FlashMessenger(
                        array('alert alert-error' => 'E-Mail konnte nicht gesendet werden. Überprüfen Sie Ihren Eingaben nochmal.'));
                    
                    
                }
                }
            }
        }
    
   
    public function newpasswordAction() {
        
        $form = new Inv_Form_Newpass;
        $this->view->form = $form;
        $this->repository = new Inv_Model_Repository();
        $request = $this->getRequest();
        
                if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                try{
               $data = $request->getPost();
               if($data['password_new'] == $data['password_again']) { 
               $user = Inv_Model_Mail::keyEmail($this->_getParam('key'));
               $this->repository->changePassword($data["password_new"], $user);
               
               $this->_helper->FlashMessenger(
               array('alert alert-success' => 'Passwort wurde efolgreich geändert!')
                    );
                }
                else throw new Exception('Passwort stimmten nicht überein!') ;
                }
                catch(Exception $e) {
                    $this->_helper->FlashMessenger(
                        array('alert alert-error' => 'Irgendwas ist schiefgelaufen!')
                    );
                    return false;
                    
                    
                }
                $this->_helper->redirector('index', 'index');
               }
    
               }
    
               }
    
    public function forgotAction() {
        $form = new Inv_Form_Forgot;
        $this->view->form = $form;
   
       $this->repository = new Inv_Model_Repository();

                $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
               $data = $request->getPost();
               if($user = $this->repository->getEm()->getRepository('Entity\User')->findOneBy(array('email' => $data['email']))){
               
               Inv_Model_Mail::sendPassword($request->getPost());
               $this->_helper->redirector('index', 'index');
               }
               else{ 
               $this->_helper->FlashMessenger(
               array('alert alert-error' => 'Dieses E-Mail ist nicht bei inventarplatzhalter.conlabz.de registriert!')); 
                       return false;
               }
                
               ;
            }
        }
        
    }
    
    
        
    /**
     * logout 
     */
    public function logoutAction()
    {
        $this->_auth->clearIdentity();
        $this->_helper->FlashMessenger('Logout erfolgreich.');
        
        $this->_forward('index');
    }
    
    protected function _process($values)
    {
        $adapter = $this->_getAuthAdapter();
        $adapter->setIdentity($values['username']);
        $adapter->setCredential($values['password']);
    
        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
    
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject(array('id', 'username'));
            $auth->getStorage()->write($user);
            return true;
        }
    
        return false;
    
    }
    

    
    protected function _getAuthAdapter()
    {
        $authAdapter = new Conlabz_Doctrine_Auth_Adapter(
            $this->_em,
            'Entity\User',
            'username',
            'password',
            'SHA1(?)'
        );
        return $authAdapter; 
    }
    
}
