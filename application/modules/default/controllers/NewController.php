<?php
class NewController extends Zend_Controller_Action
{   
    protected $repository;

    public function init()
    {
        $this->repository = new Inv_Model_Repository();
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('inventory', 'json')
                    ->addActionContext('invoice', 'json')
                    ->initContext();
    }

    /**
     * new Inventory Form / save
     */
    public function inventoryAction() 
    {
        $this->view->title = 'New inventory';
        $form = new Inv_Form_Default();
        $request = $this->getRequest();
        $form->setAction($request->getPathInfo());
        // Form submitted?
        if($request->isPost()) {
            $post = $request->getPost();
            try {
                // Form is valid
                if($form->isValid($post)) {
                
                    // Create new Inventory!
                    $invFactory = new Inv_Model_Factory_Inventory(null, $form->getValues());
                    $inv = $invFactory->createItem();
                    $this->repository->save($inv);
                    $this->_helper->Success('Inventar wurde gespeichert!', $this->view->url(
                        array('id' => $inv->getId()), 'view'
                    ));
                } else {
                    $this->view->formErrors = $form->getMessages();
                }
            } catch(Exception $e) {
                $this->view->errors = $e->getMessage();
            }
        }
        $this->view->form = $form;
    }
    
    /**
     * add new invoice
     */
    public function invoiceAction()
    {
        $form = new Inv_Form_Invoice();
        $request = $this->getRequest();
        $form->setAction($request->getPathInfo());
        $this->view->title = 'New invoice';
        if($request->isPost()) {
            $post = $request->getPost();
            try {
                // Form is valid
                if($form->isValid($post)) {
                    $invoiceId = $this->repository->saveInvoice($post);
                    $this->_helper->Success(
                        $this->view->translate('Invoice added.'),
                        $this->view->url(array('controller' => 'invoice', 'action' => 'view', 'id' => $invoiceId), 'default', true) 
                    );
                } else {
                    $this->view->formErrors = $form->getMessages();
                }
            } catch(Exception $e) {
                $this->view->errors = $e->getMessage();
            }
        }
        $this->view->form = $form;
    }
    
    /**
     * add new Entity Item
     */
    public function itemAction() {
    
        $output = array();
        $output['success'] = false;
        
        try {
            $entity = $this->_getParam('entity');
            $name   = $this->_getParam('name');
            
            $itemFactory = new Inv_Model_Factory_Item(null, $entity, $this->_request->getParams());
            $item = $itemFactory->createItem();
                      
            $this->repository->save($item);
            $output['success'] = true;
            $output['message'] = 'Element <strong>"' . $name . '"</strong> wurde angelegt';
            $output['id'] = $item->getId();
        } catch(Exception $e) {
            $output['message'] = $e->getMessage();
        }
        
        $this->_helper->json->sendJson($output);
    }
    
    /**
     *  add new account
     */
    public function accountAction()
    {
        $output = array();
        $output['success'] = false;
        
        $post   = $this->getRequest()->getPost();
        
        try {
            $account = new Entity\Account();
            $account->setNumber($post['number']);
            $account->setLabel($post['label']);
            
            $this->repository->save($account);
            $output['success'] = true;
            
            $view         = $this->getHelper('ViewRenderer')->view;
            $view->id     = $account->getId();
            $view->number = $account->getNumber();
            $view->label  = $account->getLabel();
            
            $output['message'] = $view->render('new/account-table-row.phtml');

        } catch(Exception $e) {
            $output['message'] = $e->getMessage();
        }
        
        $this->_helper->json->sendJson($output);
    }
    
    /**
     * check inventory-number
     */
    public function checkInventoryNumberAction() 
    {
        $number = $this->_getParam('number');
        $inv = $this->repository->findBy(array('id' => $number));
        $this->_helper->json->sendJson(array('count' => count($inv)));
    }
    
}


