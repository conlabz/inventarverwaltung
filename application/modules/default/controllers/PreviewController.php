<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PreviewController
 *
 * @author marcfischer
 */
class PreviewController extends Zend_Controller_Action {

    //put your code here


    public function init() {
        $this->repository = new Inv_Model_Repository();
    }

    public function previewAction() {


        $this->view->invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id'));
        $invoice = $this->repository->find('Entity\Invoice', $this->_getParam('id', 0));
        $filename = $invoice->getFile();
       
        $file = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $invoice->getFile();
        $this->view->images = Inv_Model_Preview::convert($file, $filename);

        

        Zend_Layout::getMvcInstance()->disableLayout();
    }


    public function imageAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $img = $this->_getParam("file");
        if(!is_null($img)) {
            $this->view->img = APPLICATION_PATH."/".Inv_Model_Repository::INVOICE_PATH."/".$img;
        }
    }

    public function indexAction() {
        $this->view->invoice = $this->repository->getInvoices();
    }

}

?>
