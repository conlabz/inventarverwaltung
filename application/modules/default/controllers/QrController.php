<?php 
class QrController extends Zend_Controller_Action
{
    protected $codeParams = array(
        'text' => 'http://www.conlabz.de/',
        'backgroundColor' => '#FFFFFF',
        'foreColor' => '#000000',
        'padding' => array(10, 2, 2, 2),
        'moduleSize' => 4,
        'eccLevel' => 'L',
        'version' => 5
    );
    protected $repository;
    protected $qrData;
    protected $id;
    
    public function init()
    {
        $id = $this->_getParam('id');
        $this->repository = new Inv_Model_Repository();
        
        $entity = $this->repository->find('Entity\Inventory', $id);
        $this->id = $entity->getId();
        
        $url  = $this->repository->getQrData($id);
        
        $this->codeParams['text'] = $url . $this->view->url(array(
                        'controller' => 'index',
                        'action' => 'view',
                        'category' => $this->view->seoUrl($entity->getCategory()->getName()),
                        'name' => $this->view->seoUrl($entity->getModel()->getName()),
                        'id' => $id), 'view');
        
        $this->view->codeParams = $this->codeParams;
                
        $this->_helper->layout()->disableLayout();
        #$this->_helper->viewRenderer->setNoRender(true);
        
    }
    
    public function pngAction() 
    {
        $this->view->codeParams['padding'] = 2;
        $this->view->rendererParams = array('imageType' => 'png');        
    }
    
    public function downloadAction()
    {   
        $this->view->rendererParams = array('sendResult' => true);
        $this->getResponse()
            ->setHeader('Content-type', 'application/force-download')
            ->setHeader('Content-Disposition', 'attachment;filename="Anlagegut-' . $this->id . '.png"');
    }
}
