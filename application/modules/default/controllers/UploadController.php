<?php
/**
 * Description of UploadController
 *
 * @author hummer
 */
class UploadController extends Zend_Controller_Action 
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->repository = new Inv_Model_Repository();
    }
    
    public function invoiceAction()
    {
        $upload = array();
        //$inventoryId = $this->_getParam("inventoryId",null);
        
        $id = $this->_getParam("id",null);
        if($this->_request->isPost()) {
            $data = $this->getRequest()->getPost();
            $upload = $this->repository->uploadInvoice($data,$id);
        }
        
        if($this->_request->isGet()) {
            $upload = $this->repository->getUpload($id);
        }
        
        
        
        $this->view->json = Zend_Json::encode($upload);
    }
    
    public function deleteAction()
    {
        $file = APPLICATION_PATH . '/' . 
            Inv_Model_Repository::INVOICE_PATH . '/' .
            basename($this->_getParam('file'));
        
        $invoice = $this->repository->find('Entity\Invoice',$this->_getParam('id'));
        Inv_Model_DeletingFile::delete($invoice);
        $this->repository->remove($invoice);
        
//        if(file_exists($file) && unlink($file)) {
//            $this->_helper->json->sendJson(true);
//        }
        
    }
}
