<?php

class Inv_Form_Default extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        /**
         * get Data for Autocomplete
         */
        $repository = new Inv_Model_Repository();
        // Models
        $models = $repository->getAcData('Entity\Model', array('name'));
        // Categories
        $categories = $repository->getCategoryTree();                
        // Get Person
        $persons = $repository->getAcData('Entity\Person', array('lastname','firstname'));
        // get Manufacturer
        $manufacturer = $repository->getAcData('Entity\Manufacturer', array('name'));
        // get Distributor
        $distributor = $repository->getAcData('Entity\Distributor', array('name'));
        // get Accounts
        $accounts = $repository->getAcData('Entity\Account', array('number', 'label'));
        // get Invoices
        $invoices = $repository->getAcData('Entity\Invoice', array('invoicenr'));
        // get Labels
        $labels = $repository->getAcData('Entity\Label', array('name'));
       
        
        
                
        // ------------------------------------------------------------------------
        // Category select
        $catSelect = $this->createElement('select', 'category', array('belongsTo' => 'entities','filters' => array('StripTags'),));
        $catSelect->setLabel('Category')
            ->setOptions(array('required' => true, 'description' => 'Bitte wählen Sie eine passende Kategorie'))
            ->addMultiOptions($categories);
        $this->addElement($catSelect);
       
        
        // Manufacturer Form Element
        $manuSelect = $this->createElement('select', 'manufacturer', array('belongsTo' => 'entities','filters' => array('StripTags'),));
        $manuSelect->setLabel('Manufacturer')
            ->setOptions(array('class' => 'combobox', 'description' => 'Bitte wählen Sie den richtigen Hersteller des Inventarguts.'))
            ->addMultiOptions($manufacturer);
        $this->addElement($manuSelect);
        
        // Label select
        $labelSelect = $this->createElement('select', 'label', array('belongsTo' => 'entities','filters' => array('StripTags'),));
        $labelSelect->setLabel('Label')
            ->setOptions(array('class' => 'combobox', 'required' => true, 'description' => 'Wählen Sie hier die passende Bezeichnung.'))
            ->addMultiOptions($labels);
        $this->addElement($labelSelect);
        
        // Model select
        $modelSelect = $this->createElement('select', 'model', array('belongsTo' => 'entities', 'filters' => array('StripTags'),));
        $modelSelect->setLabel('Model')
            ->setOptions(array('data-name' => 'model', 'class' => 'combobox', 'required' => true, 'description' => 'Wählen Sie hier das passende Modell.'))
            ->addMultiOptions($models);
        $this->addElement($modelSelect);
        
        // Serial
        $this->addElement('text', 'serialnr', array(
            'label' => 'Serial number',
            'belongsTo' => 'data',
            'filters' => array('StripTags'),
            'description' => 'Bitte tragen Sie hier die eindeutige Seriennummer ein, die Sie auf dem Gerät oder der Rechnung finden.'
        ));
        
        
        // ------------------------------------------------------------------------
        // Person select
        $personSelect = $this->createElement('select', 'person', array('belongsTo' => 'entities'));
        $personSelect->setLabel('Person')
            ->setOptions(array('class' => 'combobox', 'description' => 'Hier geben Sie an, wer das Anlagegut überlicherweise verwendet.'))
            ->addMultiOptions($persons);
        $this->addElement($personSelect);
        
        // Location
        $this->addElement('text', 'location', array(
            'label' => 'Location',
            
            'belongsTo' => 'data',
            
            'filters' => array('StripTags'),
            
            
            'description' => 'Hier weisen Sie dem Anlagegut einen Standort innerhalb Ihres Unternehmens zu. Mit Hilfe dieser Information finden Sie Ihre Anlagegüter im Handumdrehen wieder.'
        ));
        
        
        
        
        // ------------------------------------------------------------------------
        // Distributor Select
        $distriSelect = $this->createElement('select', 'distributor', array('belongsTo' => 'entities','filters' => array('StripTags'),));
        $distriSelect->setLabel('Distributor')
            ->setOptions(array('class' => 'combobox', 'description' => 'Bitte wählen Sie den Lieferant des Anlageguts.'))
            ->addMultiOptions($distributor);
        $this->addElement($distriSelect);
        
        // Price
        $this->addElement('text', 'price', array(
            'label' => 'Price',
            'filters' => array('Currency'),
            'belongsTo' => 'data',
            'description' => 'Geben Sie dem Anlagegut einen Nettopreis. Falls Ihr Unternehmen keine Umsatzsteuer ausweist oder Sie im Sinne des §19 UStG oder anderweitig umsatzsteuerbefreit sind, ist hier der Bruttobetrag einzutragen.'
            
        ));
        
         // status
      
        
        $this->addElement('select', 'status', array(
            'label' => 'Status',
            'multiOptions' => array('' => '', '3' => 'Gesperrt', '4' => 'Ausgebucht'),
            'belongsTo' => 'data',
            'filters' => array('StripTags'),
            'description' => 'Sie können manuell die Rechnung auf Gesperrt oder Ausgebucht (mit Kommentar) setzen.'    
        ));
        
        // Kommentar
        $this->addElement('textarea','comment',array(
            'label' => 'Kommentar',
            'filters' => array('StripTags'),
            'belongsTo' => 'data',
            'description' => 'Kommentieren',
            'cols' => '4',
            'rows' => '4',
                
        ));
        
        
        // Preview-button
       
        
        // -------------------------------------------------------------------------
        // Invoice
//        $invoiceSelect = $this->createElement('select', 'invoice', array('belongsTo' => 'entities'));
//        $invoiceSelect->setLabel('Invoice')
//            ->setOptions(array('class' => 'combobox', 'description' => 'Übernehmen Sie einfach die Rechnungsnummer aus der Originalrechnung. So können Sie das Anlagegut z.B. in Garantiefällen sofort zuordnen.'))
//            ->addMultiOptions($invoices);
//        $this->addElement($invoiceSelect);
//        

        // ------------------------------------------------------------------------
        // Account Select
        $accountSelect = $this->createElement('select', 'account', array('belongsTo' => 'entities'));
        $accountSelect->setLabel('Account')
            ->addMultiOptions($accounts);
        $this->addElement($accountSelect);
        
        // Inventory Account Number
        $invaccnr = $this->addElement('text', 'invaccnr', array(
            'label' => 'Account inventory number',
            'belongsTo' => 'data',
            'filters' => array('StripTags'),
            'data-name' => 'invaccnr',
            'description' => 'Bitte übernehmen Sie hier die Inventarnummer aus Ihrem aktuellen Restwertverzeichnis. Üblicherweise wird dieses etwas zeitverzögert erstellt. Aus diesem Grunde empfiehlt es sich, diese Daten sofort nach Erhalt der aktuellen Buchhaltungsdaten nachzutragen.',
            'validators' => array(new Conlabz_Validate_Account())
        ));
        $suggestDecorator = new Conlabz_Form_Decorator_Suggest();
        // Decorators
        foreach($this->getElements() as $element) {
            $element->setDecorators(array(
                'ViewHelper',
                array('Errors', array('class' => 'alert alert-error')),
                'FormElements',
                array('Description', array('tag' => 'p', 'class' => 'help-block')),
                array('Label', array('class' => 'control-label')),
                $suggestDecorator
            ));
            #$element->setDecorators(array('ViewHelper',new Conlabz_Form_Decorator_InputControl()));
        }
        
        $recorddate = $this->createElement('hidden', 'recorddate');
        $recorddate->setDecorators(array('ViewHelper'));
        $this->addElement($recorddate);
                
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setOptions(array(
            'label' => 'save',
            'class' => 'btn btn-success'
        ));
        
                $this->addElement('text', 'invoicenr', array(
            'label' => 'Invoice number',
           
            'data-name' => 'invoicenr',
            'belongsTo' => 'invoices'
            
        ));        
        
        $this->addElement('text', 'invoicedate', array(
            'label' => 'Invoice date',
            'belongsTo' => 'invoices'
        ));
        
        $invoicePdf = $this->createElement('hidden', 'invoice_pdf', array('belongsTo' => 'invoices'));
        $invoicePdf->setDecorators(array('ViewHelper'));
        $this->addElement($invoicePdf);
        
        $invoiceId = $this->createElement('hidden', 'invoice_id', array('belongsTo' => 'invoices'));
        $invoiceId->setDecorators(array('ViewHelper'));
        $this->addElement($invoiceId);
        
        
        
        $submit->setDecorators(array('ViewHelper'));
        $this->addElement($submit);
    }
 
}
