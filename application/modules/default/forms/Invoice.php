<?php
/**
 * @author hummer
 */
class Inv_Form_Invoice extends Zend_Form
{
    public function init()
    {
        $this->addElement('text', 'invoicenr', array(
            'label' => 'Invoice number',
            'required' => true,
            'data-name' => 'invoicenr'
        ));
        
        $this->addElement('text', 'invoicedate', array(
            'label' => 'Invoice date'
        ));
                
        foreach($this->getElements() as $element) {
            $element->setDecorators(array(
                'ViewHelper',
                array('Errors', array('class' => 'alert alert-error')),
                'FormElements',
                'Label'
            ));
        }
        
        $invoicePdf = $this->createElement('hidden', 'invoice_pdf');
        $invoicePdf->setDecorators(array('ViewHelper'));
        $this->addElement($invoicePdf);
        
        $invoiceId = $this->createElement('hidden', 'invoice_id');
        $invoiceId->setDecorators(array('ViewHelper'));
        $this->addElement($invoiceId);
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setOptions(array(
            'label' => 'save',
            'class' => 'btn btn-success'
        ));
        $submit->setDecorators(array('ViewHelper'));
        $this->addElement($submit);
        
    }
}

?>
