<?php
class Inv_Form_Login extends Zend_Form
{
    public function init()
    {

        $this->setName("login");

        $this->setMethod('post');        

        $this->addElement('text', 'username', array(
                        'filters'    => array('StringTrim', 'StringToLower'),
                        'validators' => array(
                                        array('StringLength', false, array(0, 50)),
                        ),
                        'required'   => true,
                        'description' => 'Benutzername: demo',
                        'label'      => 'Username',
                        
                        
        ));



        $this->addElement('password', 'password', array(
                        'filters'    => array('StringTrim'),
                        'validators' => array(
                                        array('StringLength', false, array(0, 50)),
                        ),
                        'decription' => 'Passwort: demo',
                        'required'   => true,
                        'label'      => 'Password',
                         
                        
        ));

        $this->addElement('submit', 'login', array(
                        'required' => false,
                        'ignore'   => true,
                        'label'    => 'Login',
                        'class'    => 'btn btn-primary login-btn'
        ));
        

    }
}