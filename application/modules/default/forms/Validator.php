<?php 
class Inv_Form_Validator
{
    protected $form;
    protected $repository;
    
    public function __construct($form)
    {
         $this->form = $form;
         $this->repository = new Inv_Model_Repository();
    }
    
    public function isValid($data)
    {
        if($this->form->isValid($data) && $this->checkAccount($data)) {
            return true;
        }
        return false;
    }
    
    public function getMessages()
    {
        return $this->form->getMessages();
    }
    
    public function checkAccount($data)
    {
        $req = Zend_Controller_Front::getInstance()->getRequest();
        
        if(!empty($data['data']['invaccnr']) && empty($data['entities']['account'])) {
            throw new Exception('Das Inventarobjekt muss einem Konto zugeordnet sein, um eine Inv-nr. (lt Rv.) festlegen zu können.');
            return false;
        } elseif(!empty($data['data']['invaccnr']) && !empty($data['entities']['account'])) {
            $dql = "SELECT    i
                    FROM      Entity\Inventory i
                    WHERE     i.id != '" . $req->getParam('id') . "'
                    AND       i.account = '" . $data['entities']['account'] . "'
                    AND       i.invaccnr = '" . $data['data']['invaccnr'] . "'";            
            $result = $this->repository->getEm()->createQuery($dql)->getResult();
            if(count($result) > 0) {
                throw new Exception('Es existiert bereits diese Kombination von Konto und Inv-Nr. (lt Rv.)');
                return false;
            }
        }
        
        return true;
    }
    
}
?>