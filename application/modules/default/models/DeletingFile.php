<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeletingFile
 *
 * @author marcfischer
 */
class Inv_Model_DeletingFile {

    public static function delete($invoice) {
        $repository = new Inv_Model_Repository();
       
        $file = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $invoice->getFile();
        $img = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $invoice->getFile() . '.jpg';
        
        $nr = 0;
        if (is_file($file) && @unlink($file)) {
            $invoice->setFile(null);
            $repository->save($invoice);
        }
        if (is_file($img) && @unlink($img)) {
         
        } else {
            while (is_file($file . "-" . $nr . ".jpg")) {
                unlink($file . "-" . $nr . ".jpg");
                $nr++;
            }
        }
        
        return;
    }

}

?>
