<?php
namespace Entity;

/**
 * @Entity
 * @Table(name="account")
 */
class Account
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $number;

    /**
     * @Column(type="string")
     */
    protected $label;

    /**
     * @OneToMany(targetEntity="Inventory", mappedBy="inventory")
     * @OrderBy({"name" = "ASC"})
     */
    protected $inventories;

    public function __construct()
    {
        $this->inventories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addInventory($inventory)
    {
        $this->inventories->add($inventory);
        $inventory->setAccount($this);
    }

    public function getInventories()
    {
        return $this->inventories;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNumber($number)
    {
        $number = trim($number);
        if(empty($number)) {
            throw new \Exception('Es muss eine Nummer angegeben werden!');
        }
        $this->number = $number;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setLabel($label)
    {
        $label = trim($label);
        if(empty($label)) {
            throw new \Exception('Es muss eine Bezeichnung angegeben werden!');
        }
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

}
