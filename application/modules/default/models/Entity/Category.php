<?php
namespace Entity;

use DoctrineExtensions\NestedSet\Node;

/**
 * @Entity
 * @Table(name="category")
 */
class Category implements Node
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $name;
    
    /**
     * @Column(type="integer")
     */
    private $lft;
    
    /**
     * @Column(type="integer")
     */
    private $rgt;

    /**
     * @OneToMany(targetEntity="Inventory", mappedBy="inventory")
     * @OrderBy({"name" = "ASC"})
     */
    protected $inventories;

    public function __construct()
    {
        $this->inventories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addInventory($inventory)
    {
        $this->inventories->add($inventory);
        $inventory->setCategory($this);
    }

    public function getInventories()
    {
        return $this->inventories;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function __toString()
    {
        return $this->name;
    }
    

    public function getLeftValue() { return $this->lft; }
    public function setLeftValue($lft) { $this->lft = $lft; }
    
    public function getRightValue() { return $this->rgt; }
    public function setRightValue($rgt) { $this->rgt = $rgt; }
}