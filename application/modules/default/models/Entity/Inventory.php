<?php

namespace Entity;

/**
 * @Entity
 * @Table(name="inventory")
 */
class Inventory {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Category", inversedBy="category", cascade={"persist"})
     */
    protected $category;

    /**
     * @ManyToOne(targetEntity="Label", inversedBy="label", cascade={"persist"})
     */
    protected $label;

    /**
     * @ManyToOne(targetEntity="Manufacturer", inversedBy="inventory", cascade={"persist"})
     */
    protected $manufacturer;

    /**
     * @ManyToOne(targetEntity="Model", inversedBy="inventory", cascade={"persist"})
     */
    protected $model;

    /**
     * @ManyToOne(targetEntity="Person", inversedBy="inventory", cascade={"persist"})
     */
    protected $person;

    /**
     * @ManyToOne(targetEntity="Distributor", inversedBy="inventory", cascade={"persist"})
     */
    protected $distributor;

    /**
     * @ManyToOne(targetEntity="Account", inversedBy="inventory", cascade={"persist"})
     */
    protected $account;

    /**
     * @Column(type="string", nullable=true)
     */
    protected $invaccnr;

    /**
     * @Column(type="string")
     */
    protected $serialnr;

    /**
     * @Column(type="string")
     */
    protected $location;

    /**
     * @Column(type="decimal")
     */
    protected $price;

    /**
     * @ManyToMany(targetEntity="Invoice", cascade={"persist"})
     * 
     **/
    protected $invoices;
           
    public function __construct() {
           
            $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
            
        }
    
    public function getInvoices() {
        return $this->invoices;
    }
    public function addInvoices(Invoice $invoice) {
        $this->invoices->add($invoice);
    }
 
    /**
     * @Column(type="date")
     */
    protected $recorddate;
    
    public function getInvoicesNr() {
        return $this->invoices->invoicenr;
    }
    /**
     *
     * @Column(type="string")
     */
    protected $status;

    /**
     *
     * @Column(type="string")
     */
    protected $comment;

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function getCategory() {
        return $this->category;
    }

    public function getCategoryPath() {
        $nsm = \Zend_Registry::get('nestedSetManager');
        $node = $nsm->wrapNode($this->category);
        $ancestors = $node->getAncestors();
        unset($ancestors[0]);
        $ancestors[] = $this->category->getName();
        return $ancestors;
    }

    public function setManufacturer($manufacturer) {
        $this->manufacturer = $manufacturer;
    }

    public function getManufacturer() {
        if (empty($this->manufacturer)) {
            return new Manufacturer();
        }
        return $this->manufacturer;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function getModel() {
        return $this->model;
    }

    public function setPerson($person) {
        $this->person = $person;
    }

    public function getPerson() {
        if (empty($this->person)) {
            return new Person();
        }
        return $this->person;
    }
    
    public function setDistributor($distributor) {
        $this->distributor = $distributor;
    }

    public function getDistributor() {
        if (empty($this->distributor)) {
            return new Distributor();
        }
        return $this->distributor;
    }

    public function setAccount($account) {
        $this->account = $account;
    }

    public function getAccount() {
        if (!empty($this->account)) {
            return $this->account;
        }
        return new Account();
    }

    public function setInvaccnr($invaccnr) {
        $this->invaccnr = $invaccnr;
    }

    public function getInvaccnr() {
        return $this->invaccnr;
    }

    public function setSerialnr($serialnr) {
        $this->serialnr = $serialnr;
    }

    public function getSerialnr() {
        return $this->serialnr;
    }

    public function setLocation($location) {
        $this->location = $location;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice() {
        return $this->price;
    }
//
//    public function setInvoice($invoice) {
//        $this->invoice->add($invoice);
//        
//    }
//
//    public function getInvoice() {
//       
//        return $this->invoice;
//    }

//    public function getInvoiceId() {
//        
//        if (null !== $this->invoice) {
//            return $this->invoice->Id();
//        }
//    }
  

    public function setInvoicedate($invoicedate) {
        $this->invoicedate = $invoicedate;
    }

    public function getInvoicedate() {
        return $this->invoicedate;
    }

    public function setRecorddate(\DateTime $recorddate) {
        $this->recorddate = $recorddate;
    }

    public function getRecorddate() {
        return $this->recorddate;
    }

    public function setComment($comment) {
        $this->comment = $comment;
    }

    public function getComment() {

        return $this->comment;
    }

    public function getStatus($asIcon = false) {
          
        
        
        
        

        if (!$asIcon) {

            $this->statusret = \Inv_Model_Status::intToStatus($this->status);

            return $this->statusret;
        } else {
            return '<i title=" '. \Inv_Model_Status::intToTitle($this->status) .' " class="' . \Inv_Model_Status::intToIcon($this->status) . '"></i>';
        }
    }

    public function setStatus($status = NULL) {

        
        $this->status = $status;
        }
        

    

    public function __toString() {
        $string = 'Inventarnummer: ' . $this->id . "\n";
        $string .= 'Kategorie: ' . $this->category->getName() . "\n";
        $string .= 'Modell: ' . $this->model->getName() . "\n";
        $string .= 'Seriennummer: ' . $this->serialnr . "\n";
        $string .= 'Person: ' . $this->person->getFirstname() . ' ' . $this->person->getLastname() . "\n";
        $string .= 'Standort: ' . $this->location . "\n";
        $string .= 'Lieferant: ' . $this->distributor->getName() . "\n";
        $string .= 'Preis:' . $this->price . "\n";
        //$string .= 'Rechnungsnummer: ' . $this->invoice . "\n";
        $string .= 'Rechnungsdatum: ' . $this->invoicedate->format('d.m.Y') . "\n";
        $string .= 'Konto: ' . $this->account->getNumber() . ', ' . $this->account->getLabel() . "\n";
        $string .= 'Nr. lt. Restwertverzeichnis: ' . $this->invaccnr . "\n";
        $string .= 'Eingetragen: ' . $this->recorddate . "\n";
        $string .= 'Status: ' . $this->status . "\n";

        return $string;
    }

    public function __call($name, $arguments) {
        // magic getter
        if (strtolower(substr($name, 0, 3)) === 'get') {
            $var = ucfirst(substr($name, 3, strlen($name)));

            $pass1 = preg_replace("/([a-z])([A-Z])/", "\\1 \\2", $var);
            $pass2 = preg_replace("/([A-Z])([A-Z][a-z])/", "\\1 \\2", $pass1);

            $methods = explode(' ', $pass2);
            $getter = 'get' . ucfirst(trim($methods[0]));
            $getter2 = 'get' . ucfirst(trim($methods[1]));
            if (method_exists($this, $getter)) {
                if (is_object($this->{$getter}())) {
                    return $this->{$getter}()->{$getter2}();
                }
                return $this->{$getter}();
            }
        }
    }

}
