<?php
namespace Entity;

/**
 * @Entity
 * @Table(name="invoice")
 */
class Invoice
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $invoicenr;

    /**
     * @Column(type="date", nullable=true)
     */
    protected $invoicedate;

    /**
     * @Column(type="string", nullable=true)
     */
    protected $file;
    
    /**
     * @Column (type="string", nullable=true) 
     * 
     */
    protected $size;
//    /**
//     * @OneToMany(targetEntity="Inventory", mappedBy="invoice")
//     * @OrderBy({"id" = "ASC"})
//     */
//    protected $inventories;
//
//    public function __construct()
//    {
//       $this->inventories = new \Doctrine\Common\Collections\ArrayCollection();
//    }
//
//    public function addInventory($inventory)
//    {
//        $this->inventories->add($inventory);
//        $inventory->setInvoice($this);
//    }
//
//    public function getInventories()
//    {
//        return $this->inventories;
//    }
    
    

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setInvoicenr($invoicenr)
    {
        $this->invoicenr = $invoicenr;
    }

    public function getInvoicenr()
    {
        return $this->invoicenr;
    }
    
    public function setSize($size) 
    {
        $this->size = $size;
    }
    public function getSize()
    {
        return $this->size;
    }

    public function setInvoicedate($invoicedate)
    {
        $this->invoicedate = !empty($invoicedate) ? new \DateTime($invoicedate) : null;
    }

    public function getInvoicedate($format = 'd.m.Y')
    {
        return is_object($this->invoicedate) ? $this->invoicedate->format($format) : '';
    }

    public function setFile($file)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

}
