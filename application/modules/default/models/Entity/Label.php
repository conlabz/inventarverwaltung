<?php
namespace Entity;

/**
 * @Entity
 * @Table(name="label")
 */
class Label
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $name;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName() 
    {
        return $this->name;
    }

}