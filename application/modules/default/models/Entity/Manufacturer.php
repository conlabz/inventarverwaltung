<?php
namespace Entity;

/**
 * @Entity
 * @Table(name="manufacturer")
 */
class Manufacturer
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $name;

    /**
     * @OneToMany(targetEntity="Inventory", mappedBy="manufacturer")
     */
    protected $inventory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inventory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addInventory($inventory)
    {
        $this->inventory->add($inventory);
        $inventory->setManufacturer($this);
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}
