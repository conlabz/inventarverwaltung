<?php
namespace Entity;

/**
 * @Entity
 * @Table(name="persons")
 */
class Person
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=45)
     */
    protected $firstname;

    /**
     * @Column(type="string", length=45)
     */
    protected $lastname;

    /**
     * @OneToMany(targetEntity="Inventory", mappedBy="person")
     */
    protected $inventory;

    public function __construct()
    {
        $this->inventory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addInventory(Inventory $inventory)
    {
        $this->inventory->add($inventory);
        $inventory->setPerson($this);
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }
}