<?php
abstract class Inv_Model_Factory_Abstract
{
    protected $entity;
    protected $repository;
    protected $data;
    
    public function __construct($entity = null, $name = 'Entity\Inventory', $data = array()) {
        
        if(substr($name, 0, 7) !== 'Entity\\') {
            $name = 'Entity\\' . ucfirst($name);
        }
        
        $this->data = $data;
        
        if(null !== $entity) {
            $this->entity = $entity;
        } else {
            $this->entity = new $name();
        }
        
        $this->repository = new Inv_Model_Repository();
    }
    
    public function setData(array $data)
    {
        $this->data = $data;
    }
    
    public function getData($key = null)
    {
        if (null !== $key) {
            return isset($this->data[$key]) ? $this->data[$key] : null;
        }
        return $this->data;        
    }
    
    abstract public function createItem();
}
