<?php
class Inv_Model_Factory_Inventory extends Inv_Model_Factory_Abstract
{  
    public function __construct($entity = null, $data = array())
    {
        parent::__construct($entity, 'inventory', $data);
    }
    
    public function getEntity()
    {
        return $this->entity;
    }
    
    /**
     * create inventory item
     * @param   post    array // Form data
     */
    public function createItem() {

        $post = $this->data;
        
        foreach($post['entities'] as $entity => $value) {
            $entityName = 'Entity\\' . ucfirst($entity);
            $methodName = 'set' . ucfirst($entity);
            $entityObj = $this->repository->find($entityName, $value);
            $this->entity->{$methodName}($entityObj);
        }
        
        foreach($post['data'] as $key => $value) {
            $methodName = 'set' . ucfirst($key);
            $this->entity->{$methodName}($value);
        }
                      
        $recorddate  = !empty($post['recorddate'])  ? new DateTime($post['recorddate']) : new DateTime();
        $this->entity->setRecorddate($recorddate);
        
        return $this->entity;
    }
}