<?php
/**
 * concrete Item Factory
 *
 * @author hummer
 */
class Inv_Model_Factory_Item extends Inv_Model_Factory_Abstract 
{
    public function createItem() 
    {
        $name = $this->getData('name');
        switch(get_class($this->entity)) {
        case 'Entity\Person':
            $this->entity->setLastname($name);
            $this->entity->setFirstname('');
            break;
        case 'Entity\Invoice':
            $this->entity->setInvoicenr($name);
            break;
        default:
            $this->entity->setName($name);
            break;
        }
        return $this->entity;
    }
}
