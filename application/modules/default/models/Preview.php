<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of preview
 *
 * @author marcfischer
 */
class Inv_Model_Preview {

    public function init() {
        $this->repository = new Inv_Model_Repository();
    }

    public static function convert($file, $filename) {

        $im = new imagick($file);

        $saveimg = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $filename . '.jpg';

        $test = shell_exec(Zend_Registry::get("convert") . ' -density 100 ' . $file . ' ' . $saveimg . '');

       
       
        $imgarray = array();
        $nr = 0;
        
        if(is_file($saveimg)) {
            $imgarray[] = $filename. ".jpg";
        }
        else
            while(is_file($file."-".$nr.".jpg")){
                
            $imgarray[] = $filename."-".$nr.".jpg";
            
            $nr++;
        
            }
            
        return $imgarray;    
//        
    }

    public static function preview() {

        $img = APPLICATION_PATH . '/' . Inv_Model_Repository::INVOICE_PATH . '/' . $invnr . '.jpg';

        return $img;
    }

}

?>
