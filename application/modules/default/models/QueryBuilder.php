<?php

class Inv_Model_QueryBuilder {

    protected $filter;
    protected $order;
    protected $dqlQueryString;
    protected $dqlQueryStringPrefix = "SELECT i FROM Entity\Inventory i ";
    protected $dqlCountStringPrefix = "SELECT count(i.id) AS num FROM Entity\Inventory i ";
    
    public function __construct($filter = array(), $order = array()) {
        if(!empty($filter)) {
            $this->filter = $filter;
        }
        if(!empty($order)) {
            $this->order = $order;
        } else {
            $this->order = array(
                'column' => 'id', 
                'order'  => 'ASC'
            );
        }
    }
    
    public function buildFilteredQuery() {
        $this->dqlQueryString = $this->dqlQueryStringPrefix;
        $this->dqlQueryString.= $this->buildJoinQuery();
        $this->dqlQueryString.= $this->buildWhereClause();
        $this->dqlQueryString.= $this->buildQueryOrdered();
        return $this->dqlQueryString;
    }
    
    public function buildCountQuery() {
        return $this->dqlCountStringPrefix . 
               $this->buildJoinQuery() .
               $this->buildWhereClause();
    }
    
    public function buildQueryOrdered() {
        $dql = '';
        switch($this->order['column']) {
            case 'person':
                $dql.= " ORDER BY pe.lastname " . $this->order['order'];
                break;
            case 'label':
            case 'category':
            case 'model':
            case 'distributor':
            case 'manufacturer':
                $dql.= " ORDER BY ".substr($this->order['column'], 0, 2).".name " . $this->order['order'];
                break;
            case 'account':
                $dql.= " ORDER BY ac.number " . $this->order['order'];
                break;
            default:
                $dql.= " ORDER BY i." . $this->order['column'] . " " . $this->order['order'];
                break;
        }
        return $dql;
    }

    protected function buildJoinQuery() {
    
        $joinDql = '';
        
        // build join for where clause
        if(!empty($this->filter)) {
            foreach($this->filter as $key => $value) {
                switch($key) {
                    case 'person':
                        $joinDql.= " JOIN i.person pe ";
                        break;
                    case 'label':
                    case 'category':
                    case 'model':
                    case 'distributor':
                    case 'manufacturer':
                    case 'account':
                        $joinDql.= " JOIN i." . $key ." ".substr($key, 0, 2)." ";
                        break;
                }  
            }
        }
        
        // build join for "orderBy"
        if(!isset($this->filter[$this->order['column']])) {
            switch($this->order['column']) {
                case 'label':
                case 'person':
                case 'category':
                case 'model': 
                case 'distributor':
                case 'manufacturer':
                case 'account':
                    $joinDql .= " LEFT JOIN i.".$this->order['column']." ".substr($this->order['column'], 0, 2). " ";
                    break;
            }
        }
        
        return $joinDql;
        
    }
    
    protected function buildWhereClause() {
        
        $dql = array();
        $whereClause = '';
        
        if(!empty($this->filter)) {
            foreach($this->filter as $key => $value) {
                switch($key) {
                    case 'person':
                        $dql[] = " (" . substr($key, 0, 2) . ".lastname LIKE '%" . $value . "%'
                                 OR " . substr($key, 0, 2) . ".firstname LIKE '%" . $value ."%') ";
                        break;
                    case 'label':
                    case 'category':
                    case 'model':
                    case 'distributor':
                    case 'manufacturer':
                   
                        $dql[] = " " . substr($key, 0, 2) . ".name LIKE '%" . $value . "%' ";
                        break;
                    case 'account':
                        $dql[] = " " . substr($key, 0, 2) . ".number LIKE '%" . $value . "%' ";
                        break;
                    case 'price':
                        $value = trim($value);
                        if(preg_match('/(^<|>)/', $value)) {
                            $op    = $value[0];
                            $dql[] = " i.price " . $op . " " . (float) substr($value, 1);
                        } else {
                            $dql[] = " i.price LIKE '%" . (float) $value . "%' ";
                        }
                        break;
                    default:
                        $dql[] = " i." . $key . " LIKE '%" . $value . "%' ";
                        break;
                }
            }
            $whereClause = ' WHERE ' . implode(' AND ', $dql);
        }
        
        return $whereClause;
    }

}
