<?php
class Inv_Model_Repository
{
    /**
     * Doctrine Entity Manager
     */
    protected $em;
    
    protected $nsm;

    protected $acData = array();
        
    public function __construct() 
    {
        $this->em = Zend_Registry::get('entityManager');
        $this->nsm = Zend_Registry::get('nestedSetManager');
    }
    
    const INVOICE_PATH = 'uploads/invoices';
    
    /**
     * get filtered data
     * @param   array   filter
     */
    public function getFiltered($filter, $order) 
    {
        $query = new Inv_Model_QueryBuilder($filter, $order);
        $dql = $query->buildFilteredQuery();
        return $this->em->createQuery($dql)
           ->getResult();
    }
    
    /**
     * find all inventory items
     */
    public function findAll($entity = 'Entity\Inventory') 
    {
        return $this->em->getRepository($entity)->findAll();
    }
    
    /**
     * return entity manager
     */
    public function getEm() 
    {
        return $this->em;
    }
    
    /**
     * find by id
     */
    public function find($entity, $id) 
    {
        return $this->em->find($entity, $id);
    }
    
    /**
     * save entity
     */
    public function save($entities) 
    {
        if(!is_array($entities)) {
            $entities = array($entities);
        }
        foreach($entities as $entity) {
            $this->em->persist($entity);
        }
        $this->em->flush();
    }
    
    /**
     * remove Entity
     */
    public function remove($entity) 
    {
        $rc = new ReflectionClass(get_class($entity));
        // if nested set, remove item with nestedset-manager
        if(array_key_exists('DoctrineExtensions\NestedSet\Node', $rc->getInterfaces())) {
            $node = $this->nsm->wrapNode($entity);
            $node->delete();
        } else {
            $this->em->remove($entity);
            $this->em->flush();
        }
    }
    
    /**
     * find by where clause
     */
    public function findOneBy($where, $repository) 
    {
        $data = $this->em->getRepository($repository)->findOneBy($where);
        return !empty($data) ? $data : new $repository;
    }
    
    public function findBy(array $where) 
    {
        return $this->em->getRepository('Entity\Inventory')->findBy($where);
    } 
    
    public function findByRep($entity, array $where) 
    {
        return $this->em->getRepository($entity)->findBy($where);
    }
       
    /**
     * get Autocomplete Data
     * @param   entity  string 
     * @param   var     string
     * @return  array
     */
    public function getAcData($entity, $getvars, $separator = ' ') 
    {
        $data = array('' => '');
        $vars = array();
        foreach($getvars as $var) {
            $vars[] = 'get' . ucfirst($var);
        }
        foreach($this->em->getRepository($entity)->findAll() as $item) {
            $str = '';
            foreach($vars as $var) {
                $str .= $item->{$var}() . $separator;
            }
            $data[$item->getId()] = trim($str);
        }
        natcasesort($data);
        return $data;
    }
    
    public function getCategoryTree()
    {
        $tree = $this->nsm->fetchTreeAsArray(1);
        unset($tree[0]);
        $output = array();
        foreach($tree as $node) {
            $output[$node->getId()] = str_repeat('--', $node->getLevel() - 1) . ' ' . $node;
        }
        return $output;
    }
    
    public function saveTree(array $tree)
    {
        $root = $this->findByRep('Entity\Category', array('name' => 'root'));
        $rootId = $root[0]->getId();
        foreach($tree as $node) {
            $itemId = $node->item_id == 'root' ? $rootId : $node->item_id;
            $entity = $this->em->find('Entity\Category', $itemId);
            $entity->setLeftValue($node->left);
            $entity->setRightValue($node->right);
            $this->em->persist($entity);
        }
        $this->em->flush();
    }
    
    /**
     * get Data for QR-Code
     */
    public function getQrData($id)
    {
        if(empty($id)) {
            return null;
        }
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost();

        return $url;
    }
    
    /**
     * get category tree
     */
    public function getCategories()
    {
        $nsm = Zend_Registry::get('nestedSetManager');
        return $nsm->fetchTreeAsArray(1);
    }
    
    /**
     * save invoice
     */
    public function saveInvoice($post)
    {
        if(empty($post['invoicenr'])) {
            throw new Exception('Invoice number must not be empty!');
        }
        
        if(!($invoice = $this->find('Entity\Invoice', $post['invoice_id']))) {
            $invoice = new Entity\Invoice();
        }
        
        $invoice->setInvoicenr($post['invoicenr']);
        $invoice->setInvoicedate($post['invoicedate']);
        $file = !empty($post['file']) ? $post['file'] : $post['invoice_pdf'];   
         
        $invoice->setFile($file);
        
        $this->save($invoice);
        return $invoice->getId();
    }
    
    
    
    public function uploadInvoice($post = null)
    {

       
        
        
        $destination = APPLICATION_PATH . '/' . self::INVOICE_PATH;
        $adapter = new Zend_File_Transfer_Adapter_Http(array('ignoreNoFile' => true));        
        $adapter->setDestination($destination);        
        $data = array();
        
        foreach($adapter->getFileInfo() as $file => $info) {
            
            $name = uniqid().'.'.$this->_getExtension($adapter->getFileName());
            $adapter->addFilter('rename', $destination.'/'.$name);

            $adapter->receive($file);
            $fileclass = new stdClass();

            $fileclass->name = $name;
            $fileclass->invoicenr = $post['invoicenr'];
            $fileclass->date = $post['invoicedate'];
            $fileclass->size = $adapter->getFileSize($file);
            $fileclass->type = $adapter->getMimeType($file);
            $fileclass->test = "wtf";
            $fileclass->delete_url = '/upload/delete/file/' . $name;
            $fileclass->delete_type = 'GET';
            //$fileclass->error = 'Datum ist falsch!';
            $fileclass->url = '/'; 
            
            
            
            $invoice = new Entity\Invoice();
        $filteredinvoice = strip_tags($post['invoicenr']);
        $invoice->setInvoicenr($filteredinvoice);
        $invoice->setInvoicedate($post['invoicedate']);
        $invoice->setSize($adapter->getFilesize($file));
        $invoice->setFile($name);
        
        $this->save($invoice);
        $id = $invoice->getId();
      
            $array = Application_Model_System_Session::getValue("ids");
            if(is_array($array)) {
                $array[] = $id;
            } else {
                $array = array();
                $array[] = $id;
            }
          Application_Model_System_Session::setValue("ids", $array);
            
  
            $data[] = $fileclass;
        }
        
       
          return $data;
    }
    
    public function getupload($inventoryid = NULL){

         
         $inventory = $this
                        ->getEm()
                        ->find('Entity\Inventory', $inventoryid);
        
         foreach($inventory->getInvoices() as $invoice) { 
            $fileclass = new stdClass();
            
            $fileclass->name = $invoice->getFile();
            $fileclass->invoicenr = $invoice->getInvoicenr();
            $fileclass->date = $invoice->getInvoicedate();
            $fileclass->size = $invoice->getSize();
           
            $fileclass->delete_url = '/upload/delete/file/' . $invoice->getFile()."/id/".$invoice->getId();
            
            
            $fileclass->delete_type = 'GET';
            //$fileclass->error = 'null'; 
             
            $data[] = $fileclass;
             
         }
         if($data != NULL || $data !="") {
         return $data;
         }
         else return NULL; 
         
       
        
    }
    
    public function changePassword($password, $user = 0) {
       

    if($user == NULL) {
        return false;
    }
      
    $sha = sha1($password);
    $user->setPassword($sha);
    $this->save($user);
    
    return true;
    
    }
   
      public function refreshStatus($inventoryid, $formvalue = array()) {


        $post = $formvalue;


        $inventory = $this
                ->getEm()
                ->find('Entity\Inventory', $inventoryid);

        foreach ($inventory->getInvoices() as $invoice) {

            if ($post['data']['status'] == '3' || $post['data']['status'] == '4') {


                if ($post['data']['status'] == '3') {
                    $inventory->setStatus("3");
                }
                if ($post['data']['status'] == '4') {
                    $inventory->setStatus("4");
                }
            } 
            else {
                
                if ($invoice->getId() != NULL && $post['data']['status'] == '') {
                    $inventory->setStatus("2");
                } else {

                    $inventory->setStatus("1");
                }
            }
        }
        
        if($inventory->getStatus() == NULL) {
            $inventory->setStatus("1");
        }

    }
    

    protected function _getExtension($file)
    {
        $segments = array_reverse(explode('.', $file));
        return $segments[0];        
    }

    public function removeTags($post) {
        
        $post = strip_tags($post, '<br />');
        return $post;
        }
    
    }

