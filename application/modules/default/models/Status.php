<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status
 *
 * @author marcfischer
 * 
 */
class Inv_Model_Status {

    /**
     * 
     * @param string $status
     * @return int
     */
    public static function statusToInt($status = '') {
        $status = strtolower($status);
        $return = null;
        if ($status == 'noch zu prüfen') {
            $return = 1;
        }
        if ($status == 'aktiv') {
            $return = 2;
        }
        if ($status == 'gesperrt') {
            $return = 3;
        }
        if ($status == 'ausgebucht') {
            $return = 4;
        }

        return $return;
    }

    /**
     * 
     * @param int $int
     * @return string
     */
    public static function intToStatus($int = 0) {
        $return = null;
        if ($int == 1) {
            $return = 'Noch zu prüfen';
        }

        if ($int == 2) {
            $return = 'Aktiv';
        }

        if ($int == 3) {
            $return = 'Gesperrt';
        }

        if ($int == 4) {
            $return = 'Ausgebucht';
        }

        return $return;
    }
    
    public static function StatusToIcon ($status = 0) {
        $status = strtolower($status);
        
        $return = null;
        if($status == 'noch zu prüfen') {
            $return = "icon-flag";
        }
        if($status == 'aktiv') {
            $return = "icon-check";
        }
        if($status== 'gesperrt') {
                $return = "icon-lock";
        }
        if ($status == 'ausgebucht') {
            $return = "icon-archive";
        }
        return $return;
    }
    
    public static function intToIcon ($int = 0) {
             
        $return = null;
        if($int == 1) {
            $return = "icon-flag";
        }
        if($int == 2) {
            $return = "icon-check";
        }
        if($int == 3) {
                $return = "icon-lock";
        }
        if ($int == 4) {
            $return = "icon-archive";
        }
        
        return $return;
    }
    
    
    public static function intToTitle($int = 0) {
        
        $return = null;
        if($int == 1) {
            $return = "Noch zu prüfen";
        }
        if($int == 2) {
            $return = "Aktiv";
        }
        if($int == 3) {
                $return = "Gesperrt";
        }
        if ($int == 4) {
            $return = "Ausgebucht";
        }
        
        return $return;
        
    }
        
        
    

}

?>
