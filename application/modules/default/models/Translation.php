<?php

/**
 * Description of Translation
 *
 * @author hummer
 */
class Inv_Model_Translation 
{
    protected $_translationFile;
    protected $_dictionary;
    
    public function __construct($translationFile = null)
    {
        if(null !== $translationFile) {
            $this->_translationFile = $translationFile;
        } else {
            $this->_translationFile = Zend_Registry::get('translationFile');
        }
    }
    
    public function getTranslations()
    {
        if(null === $this->_dictionary) {
            $this->_dictionary = array();
            $file = new SplFileObject($this->_translationFile);
            $file->setFlags(SplFileObject::READ_CSV);
            foreach ($file as $row) {
                list($original, $translated) = $row;
                $this->_dictionary[$original] = $translated;
            }
        }
        return $this->_dictionary;
    }
}
