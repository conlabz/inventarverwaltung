<?php
class Inv_View_Helper_CSVEscape extends Zend_View_Helper_Abstract
{
    /**
     *
     * @param string $string
     * @return string
     */
    public function CSVEscape($string) {
        $find = array('"', ';', "\n", '€');
        $rep  = array('', '', '', 'EUR');
        return utf8_decode(str_replace($find, $rep, $string));
    }
}
