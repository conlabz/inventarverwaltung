<?php
class Inv_View_Helper_CSVExport extends Zend_View_Helper_Abstract
{
    /**
     * set the header infos
     * @param string $filename
     */
    public function CSVExport($filename = false)
    {
        header("Content-type: application/csv");
        if($filename)
        {
            header("Content-Disposition: attachment; filename=".$filename.".csv");
        } else {
            $date = new Zend_Date();
            $filename = $date->get(Zend_Date::DAY.".".Zend_Date::MONTH.".".Zend_Date::YEAR."_".Zend_Date::HOUR.".".Zend_Date::MINUTE);
            header("Content-Disposition: attachment; filename=".$filename.".csv");
        }
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}