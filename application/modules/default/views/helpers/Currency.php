<?php
class Inv_View_Helper_Currency extends Zend_View_Helper_Abstract
{
    /**
     *
     * @param string $string
     * @return string
     */
    public function currency($string, $cssStyle = true)
    {
        $currency = new Zend_Currency('de_DE', 'EUR');
        if(!empty($string)) {
            if($cssStyle) {
                return '<div style="text-align:right;">'.$currency->toCurrency($string)."</div>";
            } else {
                return $currency->toCurrency($string);
            }
        }
    }
}
