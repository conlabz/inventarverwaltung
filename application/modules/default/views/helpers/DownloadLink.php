<?php
/**
 * Description of DownloadLink
 *
 * @author hummer
 */
class Inv_View_Helper_DownloadLink extends Zend_View_Helper_Abstract 
{
    
    public function downloadLink($id, $action = 'invoice')
    {
        return '<a class="btn btn-mini" href="/download/' . $action . '/id/' . $id . '"><i class="icon icon-download"></i> download</a>';
    }
}
