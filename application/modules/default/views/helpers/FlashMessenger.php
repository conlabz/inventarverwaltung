<?php
class Inv_View_Helper_FlashMessenger extends Zend_View_Helper_Abstract
{
	private $_flashMessenger = null;

	/**
	 * Display Flash Messages.
	 *
	 * @param  string $key Message level for string messages
	 * @param  string $template Format string for message output
	 * @return string Flash messages formatted for output
	 */
	public function flashMessenger($key = 'alert alert-info',
	$template='
                                   	<div class="flashmessenger"> 
    									<div class="%s">
                                            <a class="close" data-dismiss="alert" href="#">×</a>
    										<span class="flash-message">%s</span>
    									</div>
									</div>'
    ) {
		$flashMessenger = $this->_getFlashMessenger();

		//get messages from previous requests
		$messages = $flashMessenger->getMessages();

		//add any messages from this request
		if ($flashMessenger->hasCurrentMessages()) {
			$messages = array_merge(
			$messages,
			$flashMessenger->getCurrentMessages()
			);
			//we don't need to display them twice.
			$flashMessenger->clearCurrentMessages();
		}

		//initialise return string
		$output ='';

		//process messages
		foreach ($messages as $message)
		{
			if (is_array($message)) {
				list($key,$message) = each($message);
			}
			$output .= sprintf($template,$key,$message);
		}

		return $output;
	}

	public function _getFlashMessenger()
	{
		if (null === $this->_flashMessenger) {
			$this->_flashMessenger =
			Zend_Controller_Action_HelperBroker::getStaticHelper(
                    'FlashMessenger');
		}
		return $this->_flashMessenger;
	}
}
?>