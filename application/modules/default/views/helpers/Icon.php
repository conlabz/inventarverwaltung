<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of icon
 *
 * @author marcfischer
 */


class Inv_View_Helper_Icon extends Zend_View_Helper_Abstract {

    /**
     * 
     * @param type $icon
     * @return string | null
     */
    public function icon($icon) {

        $icon = Inv_Model_Status::StatusToIcon($icon);
        if (is_null($icon)) {
            return null;
        }
        return $icon;
    }

}

?>
