<?php
class Inv_View_Helper_NestedSetList extends Zend_View_Helper_Abstract
{
    /**
     *
     * @param     array     $tree    nested set tree
     * @return    string             ordered html list
     */
    public function nestedSetList(array $tree, $root = false, $attribs = array())
    {
        $start = 0;
        if(false === $root) {
            unset($tree[0]);
            $start = 1;
        }
        $html = "<ol class=\"sortable\">\n";
        $loops = count($tree);
        for($i = $start; $i <= $loops; $i++) {
            $currentNode = $tree[$i];
            // wenn nächster Eintrag existiert, Ebene um 1 erhöhen, sonst 0
            $nextlevel = isset($tree[$i + 1]) ? $tree[$i + 1]->getLevel() : 0;
            // Listenpunkt
            $html .= '<li id="list_' . $currentNode->getId() . '"><div><span class="editable" id="category_' . $currentNode->getId() . '_name">' . $currentNode . '</span> <a class="btn btn-danger btn-mini delete-item" href="/edit/delete-item/entity/category/id/' . $currentNode->getId() . '"><i class="icon-white icon-remove"></i> löschen</a></div>';
            // Wenn nächste Ebene größer als jetzige, neue Liste öffnen
            if($nextlevel > $currentNode->getLevel()) {
                $html .= "\n<ol>\n";
            // Wenn nächste Ebene kleiner/gleich jetzige, Listenpunkt schließen
            } elseif($nextlevel <= $currentNode->getLevel()) {
                $html .= "</li>\n";
            }
            // Anz. Stringwiederholungen
            $rep = $currentNode->getLevel() - $nextlevel;
            // Wenn nächste Ebene kleiner als jetzige, Liste und Listenpunkt schließen
            if($nextlevel < $currentNode->getLevel()) {
                $html .= str_repeat("</ol>\n</li>\n", $rep);
            }
            // restliche Listen schließen, da kein Eintrag mehr im Array vorhanden
            if(!isset($tree[$i + 1])) {
                $html .= str_repeat("</ol>\n</li>\n", $currentNode->getLevel() - $rep);
            }
        }
        $html .= '</ol>';
        return $html;
    }
}
