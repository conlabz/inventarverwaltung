<?php
class Inv_View_Helper_NestedSetSelect extends Zend_View_Helper_Abstract
{
    /**
     *
     * @param     array     $tree    nested set tree
     * @return    string             ordered html list
     */
    public function nestedSetSelect(array $tree, $attribs = array())
    {
        $html = '<select class="span2" name="parent">';
        foreach($tree as $node) {
            $html .= '<option value="' . $node->getId() . '">' . str_repeat('&nbsp;&nbsp;', $node->getLevel()) . $node . "</option>\n";
            
        }
        $html .= '</select>';
        return $html;
    }
}
