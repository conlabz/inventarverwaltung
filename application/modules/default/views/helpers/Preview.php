<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Preview
 *
 * @author marcfischer
 */
class Inv_View_Helper_Preview extends Zend_View_Helper_Abstract {

    /**
     * 
     * @param type $id
     * @return string | null
     */
    public function preview($id) {
        return '<a class="btn btn-mini" href="/preview/preview/id/' . $id . '"><i class="icon icon-tag"></i> Vorschau </a>';
    }
    


}