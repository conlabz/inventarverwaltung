<?php
class Inv_View_Helper_SeoUrl extends Zend_View_Helper_Abstract
{
    /**
     *
     * @param string $string
     * @return string
     */
    public function seoUrl($string, $cssStyle = true)
    {
        $uml = array('ä', 'ö', 'ü', 'ß');
        $rep = array('ae', 'oe', 'ue', 'ss');
        $string = str_replace($uml, $rep, $string);
        $string = preg_replace("`\[.*\]`U","",$string);
        $string = preg_replace('`&(amp;)?#?[a-z0-9._]+;`i','-',$string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string);
        $string = preg_replace(array("`[^a-z0-9._]`i","`[-]+`") , "-", $string);
        return mb_strtolower(trim($string, '-'));
    }
}
