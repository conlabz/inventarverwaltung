<?php

class Inv_View_Helper_Status extends Zend_View_Helper_Abstract {

    /**
     * 
     * @param type $status
     * @return string | null
     */
    public function status($status) {
        $status = Inv_Model_Status::statusToInt($status);
        if (is_null($status)) {
            
            return null;
        }
        return "status" . $status;
        
     
    }
    


}
