<?php
/**
 *
 * @author Julian Gilles (conlabz GmbH)
 *
 */
class Inv_View_Helper_Table extends Zend_View_Helper_Abstract
{
    /**
     *
     * dafault attribute
     * @var array
     */
    protected $_tableAttr =
    array(
        "id" 	=> "",
        "class" => ""
    );

    /**
     *
     * count the columns
     * @var int
    */
    protected $_countColumns = 0;

    /**
     *
     * @var array
     */
    protected $_columns = array();

    /**
     *
     * default options
     * @var arrary
    */
    protected $_options =
    array(
        "order" 		=> "desc",
        "export-order"  => "asc",
        "selectable" 	=> false,
        "filter"		=> false,
        "form"			=> false,
        "grouping"		=> false,
        "exportable"	=> false
    );

    /**
     *
     * defaults pagination
     * @var array
    */
    protected $_paginatorOptions =
    array(
        "active"	=> false,
        "page"		=> 1,
        "items"		=> 15
    );

    /**
     *
     * params form the request
     * @var array
    */
    protected $_params = array();

    /**
     *
     * data for the rows
     * @var array
    */
    protected $_data = array();

    /**
     *
     * table output
     * @var string
    */
    protected $_output = NULL;

    /**
     *
     * example for an array in commissions
     *
     * for normal use without a link
     * $array[$key][COLUMNNAME] = $value;
     *
     * if you want to link something
     * $array[$key][COLUMNNAME]['text'] = $valueForDisplay;
     * $array[$key][COLUMNNAME]['link'] = $valueForLink;
     *
     *
     * @param array $columns
     * @param mixed $data
     * @param boolean $isForm
     * @param mixed $attribs
     */
    public function table(array $columns, $data, $isForm = false, $attribs = false, $pagination = true, $isExportable = true)
    {

        foreach ($columns as $column)
        {
            $this->_addColumn($column);
        }

        $this->_paginatorOptions['active'] = $pagination;
        $this->_setData($data);
        $this->_options['form'] = $isForm;
        $this->_options['exportable'] = $isExportable;

        $this->_setAttribs($attribs);
        $this->_setParameters();

        return $this->__toString();
    }

    protected function _setAttribs($attribs)
    {
        if(is_array($attribs))
        {
            foreach ($attribs as $key => $value)
            {
                $method = "_set".ucfirst($key);
                if(method_exists($this, $method))
                {
                    $this->{$method}($value);
                }
            }
        }
    }

    private function _setParameters()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $this->_params['module'] 		= $request->getModuleName();
        $this->_params['action'] 		= $request->getActionName();
        $this->_params['controller'] 	= $request->getControllerName();
        $this->_params['page'] 			= $request->getParam('page');
        //$this->_params['items'] 		= $request->getParam('items');
        $this->_params['order'] 		= $request->getParam('order');
        $this->_params['column'] 		= $request->getParam('column');

        if($this->_params['module'] === "default") {
            $this->_params['url'] = $this->_params['controller']."/".$this->_params['action']."/";
        } else {
            $this->_params['url'] = $this->_params['module']."/".$this->_params['controller']."/".$this->_params['action']."/";
        }

        if($this->_params['order'] === "desc")
        {
            $this->_options['order'] = "asc";
            $this->_options['export-order'] = 'desc';
        }

        if(is_null($request->getParam('page')) || !is_null($request->getParam('column')))
        {
            $this->_params['page'] = $this->_paginatorOptions['page'];
        }

        if(is_null($request->getParam('items')))
        {
            $this->_params['items'] = $this->_paginatorOptions['items'];
        }

    }

    private function _addColumn($column)
    {
        /* @var $column Adm_Model_HtmlTable_Column */
        $this->_columns[] = $column;
        if($column->getOption("filter"))
        {
            $this->_options['filter'] = $column->getOption("filter");
        }
        if($column->getOption("grouping"))
        {
            $this->_options['grouping'] = $column->getOption("grouping");
        }
    }

    private function _setData($data)
    {
        $this->_data = $data;
    }

    private function _render()
    {
        if($this->_options['form'])
        {
            $this->_output = "<form method='post' action='/".$this->_params['url']."'>";
        }

        $this->_output.= $this->_renderFilterButton();
        $this->_output.= "<table ";
        $this->_output.= $this->_renderTableAttr();
        $this->_output.= ">";

        $this->_output.= $this->_renderTableHead();
        $this->_output.= $this->_renderFilter();
        $this->_output.= $this->_renderGrouping();
        $this->_output.= $this->_renderRows();
        $this->_output.= "</table>";
        if(!is_array($this->_data))
        {
            $this->_output.= $this->_renderPaginationControl();
        }

        if($this->_options['form'])
        {
            $this->_output .= "</form>";
        }
    }

    private function _renderTableAttr()
    {
        $output = "";
        foreach ($this->_tableAttr as $key => $value)
        {
            if(!empty($value))
            {
                $output .= " ".$key."='".$value."' ";
            }
        }
        return $output;
    }

    private function _renderTableHead()
    {
        $output = "<thead>";
        $output .= "<tr>";
        foreach ($this->_columns as $column)
        {
            /* @var $column Adm_Model_HtmlTable_Column */
            $output .= "<th ";
            $output .= $this->_renderAttr($column->getAttrs());
            $output .= "><div>";

            if($column->getOption("sortable"))
            {
                if($this->_paginatorOptions['active'])
                {
                    $output .= "<a href='/".$this->_params['url']."page/".$this->_params['page']."/column/".$column->getOption("internalName")."/order/".$this->_options['order']."'>";
                } else {
                    $output .= "<a href='/".$this->_params['url']."column/".$column->getOption("internalName")."/order/".$this->_options['order']."'>";
                }
            }
            $output.= "<span>".$column->getOption("displayName")."</span>";
            if($column->getOption("sortable"))
            {
                $output.="</a>";
                if($this->_params['column'] == $column->getOption("internalName"))
                {
                    if($this->_params['order'] == "asc")
                    {
                        $output .= ' <i class="icon-chevron-up pull-right"></i>';
                    } else {
                        $output .= ' <i class="icon-chevron-down pull-right"></i>';
                    }
                } else {
                    $output .= ' <i class="icon-minus pull-right"></i>';
                }
            }

            $output .= "</div></th>";
        }
        $output .= "</tr>";
        $output .= "</thead>";
        return $output;
    }

    private function _renderFilter()
    {
        if($this->_options['filter'])
        {
            $output = "<tr>";
            foreach ($this->_columns as $column)
            {
                /* @var $column Adm_Model_HtmlTable_Column */
                $output .= "<td>";
                if($column->getOption("filter"))
                {
                    $num = count($column->getFilters());
                    $i = 1;
                    foreach ($column->getFilters() as $filter)
                    {
                        $output .= $filter;
                        if($i > 1 && $i != $num)
                        {
                            $output.= "<br/>";
                        }
                        $i++;
                    }
                } else {
                    $output .= "&nbsp;";
                }
                $output .= "</td>";
            }
            $output .= "</tr>";
            return $output;
        }
    }

    private function _renderGrouping()
    {
        if($this->_options['grouping'])
        {
            $output = "<tr class='groupingRow'>";
            foreach ($this->_columns as $column)
            {
                /* @var $column Adm_Model_HtmlTable_Column */
                $output .= "<td>";
                if($column->getOption("grouping"))
                {
                    $output .= $column->getGrouping();
                } else {
                    $output .= "&nbsp;";
                }
                $output .= "</td>";
            }
            $output .= "</tr>";
            return $output;
        }
    }

    private function _renderFilterButton()
    {
        if($this->_options['filter'])
        {
            $translator = Zend_Registry::get('Zend_Translate');
            $output = "<div class='form-actions table-top'>";
            $output .= '<select name="itemsperpage">';
            $output .= '<option value="">'. $translator->translate('Entries per page') . '</option>';
            
            for($i = 5; $i <= 50; $i = $i + 5) {
                $output .= '<option value="' . $i . '"';
                if(Application_Model_System_Session::getValue('itemsPerPage') == $i) {
                    $output .= ' selected="selected"';
                }
                $output .= '>' . $i . '</option>';
                
            }
            $output .= '<option value="250"';
            $output .= "'> 250 </option>'";
                
            $output .= '</select> ';
            $output .= '<button class="btn" name="refresh"><i class="icon-refresh"></i> ' . $translator->translate('refresh') . '</button> ';

            if($this->_options['exportable'])
            {
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $output .= ' <a class="btn" href="/index/export/column/' . $request->getParam('column', 'category') . '/order/' . $request->getParam('order', 'asc') .'/"><i class="icon-share"></i> ' . $translator->translate('CSV-export') . '</a> ';
            }

            $output .= '<button class="btn" name="reset"><i class="icon-ban-circle"></i> ' . $translator->translate('reset') . '</button> ';
            $output .= '</div>';

            return $output;
        }
    }

    private function _renderPaginationControl()
    {
        if(!empty($this->_data))
        {
            $helper = $this->view->getHelper("paginationControl");
            return $helper->paginationControl($this->_data, 'Elastic', 'pagination.phtml');
        }
    }

    private function _renderAttr(array $data)
    {
        $output = "";
        foreach ($data as $key => $value)
        {
            $output .= $key.' = "'.$value.'"';
        }
        return $output;
    }

    private function _renderRows()
    {
        $output = "";
        if(!empty($this->_data))
        {
            foreach ($this->_data as $row)
            {
                $attr = "";
                if(isset($row['attr']))
                {
                    $attr = $this->_renderAttr($row['attr']);
                    
                }

                $output .= "<tr ".$attr.">";
               
                foreach ($this->_columns as $column)
                {
                    /* @var $column Adm_Model_HtmlTable_Column */
                    $output .= "<td ".$this->_renderAttr($column->getAttrs()).">";
                    if(isset($row[$column->getOption("internalName")]))
                    {
                        $entry 	= $row[$column->getOption("internalName")];
                        $link 	= NULL;

                        if(is_array($entry))
                        {
                            if($column->getColumnType() != "icon")
                            {
                                $link	= $entry['link'];
                                $entry 	= $entry['text'];
                            }
                        }

                        $helperName = $column->getFormatViewHelper();
                        if(!is_null($helperName))
                        {
                            $helper = $this->view->getHelper($helperName);
                            $entry = $helper->{$helperName}($entry);
                        }

                        if(!is_null($link))
                        {
                            $output.= "<a href='".$link."'>".$entry."</a>";
                        } else {

                            $output.= $entry;
                        }

                    } else {
                        $output .= "&nbsp;";
                    }
                    $output .= "</td>";
                }
                $output .= "</tr>";
            }
        }
        return $output;
    }

    /* Setter */

    private function _setClass($class)
    {
        if(!empty($class))
        {
            $this->_tableAttr["class"] = $class;
        }
    }

    private function _setId($id)
    {
        if(!empty($id))
        {
            $this->_tableAttr["id"] = $id;
        }
    }

    /**
     *
     * gibt die Tabelle aus
     */
    public function __toString()
    {
        if(is_null($this->_output))
        {
            $this->_render();
        }
        return $this->_output;
    }
}