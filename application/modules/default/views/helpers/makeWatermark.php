<?php
class Inv_View_Helper_MakeWatermark extends Zend_View_Helper_Abstract
{
    public function makeWatermark($im)
    {
        $watermark = APPLICATION_PATH . '/../public/img/conlabz-qr-watermark.png';
        $stamp = imagecreatefrompng($watermark);
        $marge_right = 0;
        $marge_bottom = 5;
        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
        imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
        return $im;
    }
    
}