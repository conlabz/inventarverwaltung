<?php

namespace App\Proxies\__CG__\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Invoice extends \Entity\Invoice implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function addInventory($inventory)
    {
        $this->__load();
        return parent::addInventory($inventory);
    }

    public function getInventories()
    {
        $this->__load();
        return parent::getInventories();
    }

    public function setId($id)
    {
        $this->__load();
        return parent::setId($id);
    }

    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setInvoicenr($invoicenr)
    {
        $this->__load();
        return parent::setInvoicenr($invoicenr);
    }

    public function getInvoicenr()
    {
        $this->__load();
        return parent::getInvoicenr();
    }

    public function setInvoicedate($invoicedate)
    {
        $this->__load();
        return parent::setInvoicedate($invoicedate);
    }

    public function getInvoicedate($format = 'd.m.Y')
    {
        $this->__load();
        return parent::getInvoicedate($format);
    }

    public function setFile($file)
    {
        $this->__load();
        return parent::setFile($file);
    }

    public function getFile()
    {
        $this->__load();
        return parent::getFile();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'invoicenr', 'invoicedate', 'file', 'inventories');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields AS $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}