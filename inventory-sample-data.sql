-- conlabz inventory sample data 


--
-- Daten für Tabelle `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(2, 'Maus'),
(3, 'Rechner'),
(4, 'Tastatur'),
(5, 'Scanner'),
(6, 'Headset'),
(7, 'Monitor'),
(8, 'Grafiktablett'),
(9, 'Stuhl'),
(10, 'Tisch'),
(11, 'Whiteboard'),
(13, 'Schrank'),
(14, 'Haushaltsgerät'),
(15, 'Trackpad'),
(17, 'Telefon'),

-- --------------------------------------------------------

--
-- Daten für Tabelle `distributor`
--

INSERT INTO `distributor` (`id`, `name`) VALUES
(1, 'Saturn Koblenz 2'),
(2, 'Büromarkt Böttcher AG'),
(3, 'marsmedia GmbH'),
(4, 'HTM GmbH'),
(5, 'designfunktion'),
(6, 'Büromöbel König'),
(7, 'Amazon EU S.a.r.L');

-- --------------------------------------------------------


--
-- Daten für Tabelle `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1, 'Apple'),
(2, 'Canon'),
(3, 'Logitech'),
(4, 'Dell'),
(5, 'Labtec'),
(6, 'Wacom'),
(7, 'LG'),
(8, 'Wagner'),
(9, 'Topstar'),
(10, 'USM Haller'),
(11, 'magnetoplan'),
(12, 'HP'),
(13, 'PAX Ikea'),
(14, 'Samsung'),
(15, 'Brother'),
(16, 'Siemens'),
(17, 'Rowenta'),
(18, 'Vitrapoint'),
(19, 'Microsoft');

-- --------------------------------------------------------

--
-- Daten für Tabelle `model`
--

INSERT INTO `model` (`id`, `name`) VALUES
(1, 'Magic Mouse'),
(3, 'Whiteboard - Schreibtafel ferroscript, einseitig'),
(5, 'Sync Master SA350'),
(6, 'Bürotisch 0,75 x 2m'),
(7, 'Senseo HD7820'),
(8, 'Bürotisch 1m x 2m'),
(9, 'Magic Trackpad'),
(10, 'MC184D/B Wireless Keyboard'),
(11, 'Gigaset S795'),
(12, 'Sync Master BX2335'),
(13, 'SK-8185'),
(14, 'Wheel Mouse Optical USB (X08-99491)'),
(15, 'DCP-357C'),
(16, 'MB110D/B Keyboard'),
(17, 'IMac12,2Inch'),
(18, 'Topstar Sitness 10, Bürostuhl'),
(19, 'Visasoft - Besucher- und Konferenzstuhl'),
(20, 'Milano Therm - Kaffeemaschine(CT 273)'),
(21, 'Executive editon - Wasserkocher'),
(22, 'K120'),
(23, 'Optiplex 755'),
(24, 'Officejet Pro 8500 A910 (CM756A)'),
(25, 'Sideboard'),
(26, 'MacBook Pro'),
(27, 'P-touch 1280');

-- --------------------------------------------------------

--
-- Daten für Tabelle `persons`
--

INSERT INTO `persons` (`id`, `firstname`, `lastname`) VALUES
(8, 'Daniel', 'Schmidt'),
(9, 'Timo', 'Herborn'),
(10, 'Julian', 'Gilles'),
(11, 'Johann', 'Carstens'),
(12, 'Donjeta', 'Veseli'),
(13, 'Adrian', 'Moundi'),
(15, 'Christian', 'Schneider'),
(17, 'Alex', 'Lyzun');
