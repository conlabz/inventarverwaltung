<?php

/**
 * Description of ContextResponse
 *
 * @author hummer
 */
class Conlabz_Controller_Action_Helper_Success extends Zend_Controller_Action_Helper_Abstract
{
    public function sendResponse($message = '', $url = '/')
    {
        $actionController = $this->getActionController();
        $req = $this->getRequest();
        
        $msg = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $msg->addMessage(array(
            'alert alert-success' => $message
        ));
        
        if($req->isXmlHttpRequest()) {
            $actionController->view->redirect = $url;
        } else {
            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
            $redirector->gotoUrl($url);
        }
        
    }
    
    public function direct($message = '', $url = '/') 
    {
        $this->sendResponse($message, $url);
    }
}
