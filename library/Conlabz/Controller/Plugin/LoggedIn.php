<?php
class Conlabz_Controller_Plugin_LoggedIn extends Zend_Controller_Plugin_Abstract 
{
    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {   
        $auth = Zend_Auth::getInstance();
        if($request->getActionName() == 'forgot' || $request->getActionName() == 'newpassword' || $request->getActionName() == 'regist') {
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout('empty');
        }
        else if(!$auth->hasIdentity()) {
 
            $request->setControllerName('login');
            $request->setActionName('index');
             
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout('empty');
        }
    }
}
