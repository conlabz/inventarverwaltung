<?php
/**
 * Input Control Decorator
 *
 * @author hummer
 */
class Conlabz_Form_Decorator_Suggest extends Zend_Form_Decorator_Abstract
{
    //<span id="entities-label-suggest" class="suggest" data-entity="label"></span>
    protected $_format = '<span id="%s-suggest" class="suggest" data-entity="%s"></span>';
    
    public function render($content)
    {
        $element = $this->getElement();
        $name = $element->getName();
        $id   = htmlentities($element->getId());
        
        $markup = sprintf($this->_format, $id, $name);
        
        $placement = $this->getPlacement();
        $separator = $this->getSeparator();
        
        switch($placement) {
            case self::PREPEND:
                return $markup . $separator . $content;
            case self::APPEND:
            default:
                return $content . $separator . $markup;
        }
    }
}
