<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Paginator
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Array.php 23775 2011-03-01 17:25:24Z ralph $
 */

/**
 * @see Zend_Paginator_Adapter_Interface
 */
require_once 'Zend/Paginator/Adapter/Interface.php';

/**
 * @category   Zend
 * @package    Zend_Paginator
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Conlabz_Paginator_Adapter_Doctrine implements Zend_Paginator_Adapter_Interface
{
    /**
     * Array
     *
     * @var array
     */
    protected $_em = null;

    /**
     * Item count
     *
     * @var integer
     */
    protected $_count = null;
    
    /**
     * Dql query-string
     * 
     * @var string
     */
    protected $_dql = '';

    /**
     * Constructor.
     *
     * @param string $dql
     * @param string $entity 
     * @param object $em     EntityManager
     */
    public function __construct($dql, $countDql, $em = null)
    {
        if(null !== $em) {
            $this->_em = $em;
        } else {
            if (Zend_Registry::isRegistered('entityManager')) {
                $this->_em = Zend_Registry::get('entityManager');
            } else {
                throw new Exception('Doctrine Entity-manager not found');
            }
        }
        
        $this->_dql   = $dql;
        
        $this->_count = (int) $this->_em->createQuery($countDql)->getSingleScalarResult();
    }

    /**
     * Returns an array of items for a page.
     *
     * @param  integer $offset Page offset
     * @param  integer $itemCountPerPage Number of items per page
     * @return array
     */
    public function getItems($offset, $itemCountPerPage)
    {
        return $this->_em->createQuery($this->_dql)
                    ->setMaxResults($itemCountPerPage)
                    ->setFirstResult($offset)
                    ->getResult();
    }

    /**
     * Returns the total number of rows in the array.
     *
     * @return integer
     */
    public function count()
    {
        return $this->_count;
    }
}