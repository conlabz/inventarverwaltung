<?php
/**
 * Description of Account
 *
 * @author hummer
 */
class Conlabz_Validate_Account extends Zend_Validate_Abstract
{
    const INVALIDACCNR = 'invalidaccnr';
    
    protected $_messageTemplates = array(
        self::INVALIDACCNR => 'Invalid Account number.'
    ); 
    
    public function isValid($value)
    {
        $em  = Zend_Registry::get('entityManager');
        $req = Zend_Controller_Front::getInstance()->getRequest();        
        $reqs = $req->getParams();
                
        $dql = "SELECT    i
                FROM      Entity\Inventory i
                WHERE     i.id != '" . $req->getParam('id', 0) . "'
                AND       i.account = '" . $reqs['entities']['account'] . "'
                AND       i.invaccnr = '" . $value . "'";
        $result = $em->createQuery($dql)->getResult();
        if(count($result) > 0) {
            $this->_error(self::INVALIDACCNR);
            return false;
        }
        return true;
    }
}
