<?php
class Zend_Filter_Currency implements Zend_Filter_Interface
{
    public function filter($value)
    {
        return number_format($value, 2, '.', '');
    }
}
