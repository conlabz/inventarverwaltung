/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    $("#comment").hide();
    $('#data-status').change(function() {
        

        $("#comment").fadeOut("slow");
        var verg = $('#data-status').val();
        if (verg == "4") {
            $("#comment").fadeIn("slow");
        }
        else
            $("#comment").fadeOut("slow");

    });
    
    //$(".help-block").hide();
    
    if($("#delete").hasClass('ison')) {
    $('.submittest').hide();
    } 
    
    $('.carousel').carousel({
        interval: false
    });
    
    $('#modal').modal({show:false});
    $("#preview").click(loadPreview);
       
    $('input[name="invoices[invoicedate]"]').datepicker({
    	dateFormat: 'dd.mm.yy',
    	changeMonth: true,
		changeYear: true
    });
    
        $('input[name="test"]').datepicker({
    	dateFormat: 'dd.mm.yy',
    	changeMonth: true,
		changeYear: true
    });
});

function loadPreview() {
   
    
    $.post('/preview/preview/id/' + $("#id").val(), function(data) {
        
        $('#modal .modal-body p').html(data);
        $('#modal').modal("show");
        rescale();
         
    });
}


function rescale(){
    var size = {width: $(window).width() , height: $(window).height() }
    /*CALCULATE SIZE*/
    var offset = 0;
    var offsetBody = 150;
    
    $('#modal').css('height', size.height - offset );
    $('.modal-body').css('max-height', size.height - (offset + offsetBody));
    $('#modal').css('top', 250);
    $('#model').css('width',700);
    
    
}

