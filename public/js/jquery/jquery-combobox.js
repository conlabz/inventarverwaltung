(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {
            var input,
                self = this,
                select = this.element.hide(),
                selected = select.children( ":selected" ),
                value = selected.val() ? selected.text() : "",
                link = $(select).closest('.controls').find('.combo-edit'),
                linkHref = link.attr('href'),
                wrapper = $( "<span>" )
                    .addClass( "ui-combobox input-append" )
                    .insertBefore( select );

            input = $( "<input>" )
                .appendTo( wrapper )
                .val( value )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: function( request, response ) {
                        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                        response( select.children( "option" ).map(function() {
                            var text = $( this ).text();
                            if ( this.value && ( !request.term || matcher.test(text) ) )
                                return {
                                    label: text.replace(
                                        new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>" ),
                                    value: text,
                                    option: this
                                };
                        }) );
                    },
                    select: function( event, ui ) {
                        ui.item.option.selected = true;                        
                        if(link.length > 0 && ui.item.option.value != '') {
                            link.attr('href', linkHref + '/id/' + ui.item.option.value).fadeIn();
                        } 
                        self._trigger( "selected", event, {
                            item: ui.item.option
                        });
                    },
                    change: function( event, ui ) {
                        if ( !ui.item ) {
                            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                                valid = false;
                            select.children( "option" ).each(function() {
                                if ( $( this ).text().match( matcher ) ) {
                                    this.selected = valid = true;
                                    return false;
                                }
                            });
                            if ( !valid ) {
                                
                                var suggestHtml = '<span class="alert alert-info">Element <strong>"' + $(this).val() + '"</strong> exisitiert nicht, anlegen?</span><br>'
                                                + '<button class="btn btn-mini new-item-yes">ja</button> <button class="btn btn-mini new-item-no">abbrechen</button>';
                                
                                var name =  $(this)
                                                .closest('.ui-combobox')
                                                .next('select').attr('id');
                                               
                                var $suggest = $('#' + name + '-suggest');
                                    $suggest.html(suggestHtml).slideDown();
                                    
                                return false;
                            }
                        }
                    }
                })

            input.data( "autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a>" + item.label + "</a>" )
                    .appendTo( ul );
            };

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .attr( "title", "Show All Items" )
                .appendTo( wrapper )
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "btn dropdown-toggle" )
                .attr('data-toggle', 'dropdown')
                .html('<span class="caret"></span>')
                .click(function() {
                    // close if already visible
                    if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                        input.autocomplete( "close" );
                        return;
                    }

                    // work around a bug (likely same cause as #5265)
                    $( this ).blur();

                    // pass empty string as value to search for, displaying all results
                    input.autocomplete( "search", "" );
                    input.focus();
                });
        },

        destroy: function() {
            this.wrapper.remove();
            this.element.show();
            $.Widget.prototype.destroy.call( this );
        }
    });
})( jQuery );
