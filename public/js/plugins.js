/*
 * jQuery i18n plugin
 * @requires jQuery v1.1 or later
 *
 * See http://recursive-design.com/projects/jquery-i18n/
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Version: 1.0.0 (201210141329)
 */
(function($) {
    /**
   * i18n provides a mechanism for translating strings using a jscript dictionary.
   *
   */

    /*
   * i18n property list
   */
    $.i18n = {
	
        dict: null,
	
        /**
     * setDictionary()
     *
     * Initialises the dictionary.
     *
     * @param  property_list i18n_dict : The dictionary to use for translation.
     */
        setDictionary: function(i18n_dict) {
            this.dict = i18n_dict;
        },
	
        /**
     * _()
     *
     * Looks the given string up in the dictionary and returns the translation if 
     * one exists. If a translation is not found, returns the original word.
     *
     * @param  string str           : The string to translate.
     * @param  property_list params : params for using printf() on the string.
     *
     * @return string               : Translated word.
     */
        _: function (str, params) {
            var result = str;
            if (this.dict && this.dict[str]) {
                result = this.dict[str];
            }
  		
            // Substitute any params.
            return this.printf(result, params);
        },

        /*
     * printf()
     *
     * Substitutes %s with parameters given in list. %%s is used to escape %s.
     *
     * @param  string str    : String to perform printf on.
     * @param  string args   : Array of arguments for printf.
     *
     * @return string result : Substituted string
     */
        printf: function(str, args) {
            if (!args) return str;

            var result = '';
            var search = /%(\d+)\$s/g;
		
            // Replace %n1$ where n is a number.
            var matches = search.exec(str);
            while (matches) {
                var index = parseInt(matches[1], 10) - 1;
                str       = str.replace('%' + matches[1] + '\$s', (args[index]));
                matches   = search.exec(str);
            }
            var parts = str.split('%s');

            if (parts.length > 1) {
                for(var i = 0; i < args.length; i++) {
                    // If the part ends with a '%' chatacter, we've encountered a literal
                    // '%%s', which we should output as a '%s'. To achieve this, add an
                    // 's' on the end and merge it with the next part.
                    if (parts[i].length > 0 && parts[i].lastIndexOf('%') == (parts[i].length - 1)) {
                        parts[i] += 's' + parts.splice(i + 1, 1)[0];
                    }
  				
                    // Append the part and the substitution to the result.
                    result += parts[i] + args[i];
                }
            }
		
            return result + parts[parts.length - 1];
        }

    };

    /*
   * _t()
   *
   * Allows you to translate a jQuery selector.
   *
   * eg $('h1')._t('some text')
   * 
   * @param  string str           : The string to translate .
   * @param  property_list params : Params for using printf() on the string.
   * 
   * @return element              : Chained and translated element(s).
  */
    $.fn._t = function(str, params) {
        return $(this).text($.i18n._(str, params));
    };

})(jQuery);

/**
 * Combobox
 */
(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {
            var input,
                self = this,
                select = this.element.hide(),
                selected = select.children( ":selected" ),
                value = selected.val() ? selected.text() : "",
                link = $(select).closest('.controls').find('.combo-edit'),
                linkHref = link.attr('href'),
                wrapper = $( "<span>" )
                    .addClass( "ui-combobox input-append" )
                    .insertBefore( select );

            input = $( "<input>" )
                .appendTo( wrapper )
                .val( value )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: function( request, response ) {
                        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                        response( select.children( "option" ).map(function() {
                            var text = $( this ).text();
                            if ( this.value && ( !request.term || matcher.test(text) ) )
                                return {
                                    label: text.replace(
                                        new RegExp(
                                            "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                        ), "<strong>$1</strong>" ),
                                    value: text,
                                    option: this
                                };
                        }) );
                    },
                    select: function( event, ui ) {
                        ui.item.option.selected = true;                        
                        if(link.length > 0 && ui.item.option.value != '') {
                            link.attr('href', linkHref + '/id/' + ui.item.option.value).fadeIn();
                        } 
                        self._trigger( "selected", event, {
                            item: ui.item.option
                        });
                    },
                    change: function( event, ui ) {
                        if ( !ui.item ) {
                            var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
                                valid = false;
                            select.children( "option" ).each(function() {
                                if ( $( this ).text().match( matcher ) ) {
                                    this.selected = valid = true;
                                    return false;
                                }
                            });
                            if ( !valid ) {
                                
                                var suggestHtml = '<span class="alert alert-info">Element <strong>"' + $(this).val() + '"</strong> exisitiert nicht, anlegen?</span><br>'
                                                + '<button class="btn btn-mini new-item-yes">ja</button> <button class="btn btn-mini new-item-no">abbrechen</button>';
                                
                                var name =  $(this)
                                                .closest('.ui-combobox')
                                                .next('select').attr('id');
                                               
                                var $suggest = $('#' + name + '-suggest');
                                    $suggest.html(suggestHtml).slideDown();
                                    
                                return false;
                            }
                        }
                    }
                })

            input.data( "autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a>" + item.label + "</a>" )
                    .appendTo( ul );
            };

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .attr( "title", "Show All Items" )
                .appendTo( wrapper )
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "btn dropdown-toggle" )
                .attr('data-toggle', 'dropdown')
                .html('<span class="caret"></span>')
                .click(function() {
                    // close if already visible
                    if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
                        input.autocomplete( "close" );
                        return;
                    }

                    // work around a bug (likely same cause as #5265)
                    $( this ).blur();

                    // pass empty string as value to search for, displaying all results
                    input.autocomplete( "search", "" );
                    input.focus();
                });
        },

        destroy: function() {
            this.wrapper.remove();
            this.element.show();
            $.Widget.prototype.destroy.call( this );
        }
    });
})( jQuery );

/**
 * AJAX Form Validation
 */
(function($, window, document, undefined) {
    "use strict";
    
    var pluginName = "ajaxFormValidation",
        that,
        defaults = {
            errorClass      :   'alert alert-error',
            group           :   '.control-group',
            groupErrorClass :   'error',
            action          :   null,
            qry             :   '/format/json',
            speed           :   'fast',
            paddingTop      :   110,
            inputSelector   :   '[name="{{input}}"], [name*="\[{{input}}\]"]',
            template        :   {
                wrapTag     :   'ul',
                msgTag      :   'li'
            },
            onError         :   function(response) {
                $(window).scrollTop(
                    $(that.getStillExistingErrors() + ':first').position().top - that.options.paddingTop
                );
            },
            onFailure       :   function(response) {
                alert('Errorhandling failed.:\n\n' + response);
            },
            onSuccess       :   function(response) {
                alert('Form saved.');
            },
            onRedirect      :   function(response) {
                $(that.element).find(':input').attr('disabled', true);
            }
        };

    function AjaxFormValidation(element, options) {
        that = this;
        this.element = element;
        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    AjaxFormValidation.prototype = {
        
        init: function() {
            $(this.element).submit(function(e) {
                e.preventDefault();
                var action = that.options.action === null ? $(this).attr('action') + that.options.qry : that.options.action + that.options.qry;
                $.post(action, $(this).serialize(), function(xhr) {
                    that.processFormResponse(xhr);
                }, 'json');
            });
        },
        processFormResponse: function(xhr) {
            this.errors = {};
            switch(true) {
                case xhr.hasOwnProperty('formErrors'):
                    this.processError(xhr);
                    break;
                case xhr.hasOwnProperty('redirect'):
                    this.hideErrors();
                    this.options.onRedirect(xhr);
                    window.location.href = xhr.redirect;
                    break;
                case xhr.hasOwnProperty('success'):
                    this.hideErrors();
                    this.options.onSuccess(xhr);
                    break;
                case xhr.hasOwnProperty('errors'):
                case xhr.hasOwnProperty('error'):
                default:
                    this.options.onFailure(xhr);
                    break;
            }
        },
        processError: function(xhr) {
            this.errors = xhr.formErrors;
            this.hideErrors();
            this.showErrors();
            this.options.onError(xhr);
        },
        hideErrors: function() {
            $(this.options.template.wrapTag + '.afv-error')
                .not(this.getStillExistingErrors())
                .slideUp(this.options.speed, function() {
                    $(this).remove();
            }).closest(this.options.group).removeClass(this.options.groupErrorClass);
        },
        getStillExistingErrors: function() {
            var errors = [];
            for(var input in this.errors) {
                errors.push('#afv-error-' + input);
            }
            return errors.join(',');
        },
        showErrors: function() {
            $.each(this.errors, function(input, messages) {
                var $input     = $(that.getInputSelector(input)),
                    $group     = $input.closest(that.options.group),
                    $errorElem = $group.find(that.options.template.wrapTag + '.afv-error'),
                    $errorMsg  = that.getHtmlError(input, messages);

                if(!$errorElem.length) {
                    $input.after($errorMsg);
                    $errorMsg.slideDown(that.options.speed);
                    $group.addClass(that.options.groupErrorClass);
                } else {
                    that.updateErrors($errorElem, messages);
                }
            });
        },
        updateErrors: function($element, messages) {
            $element.html(that.getInnerHtmlError(messages));
        },
        getInputSelector: function(input) {
            return this.options.inputSelector.replace(/{{input}}/gi, input);
        },
        getInnerHtmlError: function(messages) {
            var html = '',
                tpl  = this.options.template;
            $.each(messages, function(errorCode, message) {                  
                html += '<' + tpl.msgTag + ' class=' + errorCode + '>'
                     +  message
                     +  '</' + tpl.msgTag + '>';
            });
            return html;
        },
        getHtmlError: function(id, messages) {
            return  $('<' + this.options.template.wrapTag + '>', {
                        'id': 'afv-error-' + id,
                        'class': 'afv-error ' + this.options.errorClass
                    })
                    .html(this.getInnerHtmlError(messages))
                    .hide();
        }
    };

    $.fn[pluginName] = function (options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new AjaxFormValidation(this, options));
            }
        });
    };

})(jQuery, window, document);

/**
 * DataTable
 *
 * Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
    "sWrapper": "dataTables_wrapper form-inline"
} );

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}

/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };

            $(nPaging).addClass('pagination').append(
                '<ul>'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },

        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }

                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }

                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

/**
 * check inventory number
 */
(function($) {
	
	$.fn.checkInvNumber = function() {
		return this.each(function() {
			
			var defaultValue = $(this).val();
			
			$(this).change(function() {
				var $this = $(this),
					number = $this.val(),
					$submit = $('input#submit'),
					$controlGroup = $this.closest('.control-group');
					$helpBlock = $controlGroup.find('.help-block');
				
				if(number == defaultValue) {
					$controlGroup.removeClass('error').find('i.icon-ok').remove();	
					$submit.attr('disabled', false);
					$helpBlock.html('');
				} else {
					$.getJSON('/new/check-inventory-number/number/' + number, function(xhr) {
						if(xhr.count > 0) {
							$controlGroup.addClass('error').find('i.icon-ok').remove();
							$submit.attr('disabled', true);
							$helpBlock.html('<strong>Fehler:</strong> Diese Inventarnummer existiert bereits!');
							
						} else {
							$controlGroup.removeClass('error');	
							$submit.attr('disabled', false);
							$helpBlock.html('');
							if(!$controlGroup.find('i.icon-ok').length) {
								$this.after(' <i class="icon-ok"></i>');
							}
						}
					});
				}
			});
			
			if($.trim($(this).val()) != '') {
				var $button = $('<i class="icon-pencil"></i>').click(function(e) {
					if(confirm('Warnung: Inventarnummer wirklich ändern?')) {
						$(this)
							.fadeOut()
							.prev()
							.attr('readonly', false);
					}
				}).css({
					'margin-left': '10px',
					'cursor': 'pointer'
				}).attr('title', 'bearbeiten');
				$(this)
					.attr('readonly', true)
					.after($button);
			}
		});
	}
	
})(jQuery);

/**
 * new account ajax form submit
 */
(function($) {
	$.fn.newAccount = function() {
		$(this).submit(function(e) {
			e.preventDefault();
			var $form = $(this),
				url	  = $form.attr('action'),
				data  = $form.serialize();
			$.post(url, data, function(xhr) {
				if(!xhr.success) {
					showModal('Fehler', xhr.message);	
				} else {
					var $row = $(xhr.message).css('display', 'none');
					$row.appendTo('#accounts-table').fadeIn();
					$form[0].reset();
				}
			});
		});
	};
})(jQuery);

/**
 * check account number input
 */
(function($) {
	$.fn.checkAccInput = function(controlElem) {
		
		var $controlElem = $(controlElem),
			$invaccnr    = $(this),
			
			checkIt		 = function() {
				if($controlElem.val() == '') {
					$invaccnr
						.attr('readonly', true)
						.data('defaultValue', $invaccnr.val())
						.val('');
				} else {
					$invaccnr
						.attr('readonly', false)
						.val($invaccnr.data('defaultValue'));
				}
			};
			
		$invaccnr.data('defaultValue', $invaccnr.val());
		
	    checkIt();
		
		return $invaccnr.each(function() {
			$controlElem.change(checkIt);
		});
	};
})(jQuery);




/**
 * modal
 */
function showModal(headline, body) {
    var $modal = $('#modal');
    $modal.find('h3').text(headline);
    $modal.find('.modal-body').html(body);
    $modal.modal('show');
}
