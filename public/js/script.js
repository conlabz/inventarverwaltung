$(function() {

    // Fileupload
    var url = "/upload/invoice/id/" + $("#inventoryId").val();


    $('#upload').fileupload({
        url: url,
        acceptFileTypes: /(\.|\/)(pdf)$/i
    });

//    $('form').each(function() {
//        var that = this;
    $.getJSON(url, function(result) {
//            console.log(result);
        if (result.length > 0) {
            $('#upload').fileupload('option', 'done').call('#upload', null, {result: result});
           
        }
    });
//    });
    $('#upload').bind('fileuploadadded', function(e, data) {
       
        $('.datepicker').datepicker({
        dateFormat: 'dd.mm.yy',
        changeMonth: true,
        changeYear: true});
        
        

    });
    
    $("#upload").bind('fileuploadsubmit', function(e, data) {
       
        var date = $('.datepicker').val();
        
        
        var n = date.split(".")
        if(n[0] < 32 && n[0] > 0) {
            if(n[1] < 13 && n[1] > 0) {
                if (n[2] < 2300 && n[2] > 1970) {
                   
                    
            var inputs = data.context.find(':input');
            data.formData = inputs.serializeArray();
            return true;
            } else {
                alert("Jahr ist falsch! dd.mm.yyyy");
                return false;}
          } else {  
              alert("Monat ist falsch! dd.mm.yyyy");
              return false; }
        }
        else   alert("Datum ist falsch! dd.mm.yyyy"); return false;
        

    });

    // Delete form
    $('form.delete').submit(function(e) {
        return confirm('Wirklich löschen?');
    });

    // AJAX Form Validation
    $('form.ajax').ajaxFormValidation();

    // AJAX loader image
    $('body').append('<div id="ajax-loader"></div>');

    // confirm
    $('input[name="delete"]').click(function() {
        return confirm('Wirklich löschen?');
    });


    $('td.style')

    $('table.table-items').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sProcessing": "Bitte warten...",
            "sLengthMenu": "_MENU_ Einträge anzeigen",
            "sZeroRecords": "Keine Einträge vorhanden.",
            "sInfo": "_START_ bis _END_ von _TOTAL_ Einträgen",
            "sInfoEmpty": "0 bis 0 von 0 Einträgen",
            "sInfoFiltered": "(gefiltert von _MAX_  Einträgen)",
            "sInfoPostFix": "",
            "sSearch": "Suchen",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Erster",
                "sPrevious": "Zurück",
                "sNext": "Vor",
                "sLast": "Letzter"
            }
        }
    });


    $('#ajax-loader').ajaxStart(function() {
        $(this).fadeIn('fast');
    }).ajaxStop(function() {
        $(this).hide();
    });

    // datepicker
    $('input[name="invoicedate"]').datepicker({
        dateFormat: 'dd.mm.yy',
        changeMonth: true,
        changeYear: true
    });



    // Combobox
    $('.combobox').combobox();
    $('.ui-combobox a').removeAttr('data-toggle');

    // Suggestion
    $('.suggest').on('click', '.new-item-no', function(e) {

        e.preventDefault();
        var $suggest = $(this).closest('.suggest'),
                $controls = $(this).closest('.controls');
        $suggest.slideUp();
        $controls
                .find('input.ui-autocomplete-input')
                .val('');

    }).on('click', '.new-item-yes', function(e) {

        e.preventDefault();
        var $suggest = $(this).closest('.suggest'),
                $combobox = $suggest.closest('.suggest-elem').find('.ui-combobox'),
                $select = $combobox.next('select'),
                name = $combobox.find('.ui-autocomplete-input').val(),
                entity = $suggest.attr('data-entity'),
                url = '/new/item/entity/' + entity + '/name/' + name;

        $.getJSON(url, function(xhr) {
            if (xhr.success == true) {
                var $option = $('<option value="' + xhr.id + '">' + name + '</option>'),
                        $link = $suggest.closest('.suggest-elem').find('.combo-edit');
                $select
                        .find('option:selected')
                        .attr('selected', false);
                $select.append($option);
                $option.attr('selected', 'selected');
                $successMsg = $('<span class="label label-success">' + xhr.message + '</span>');
                $suggest.html($successMsg).delay(3000).slideUp();

                if ($link.length > 0) {
                    var href = $link.attr('href');
                    if (href.indexOf('/id/') === -1) {
                        href = href + '/id/' + xhr.id;
                    } else {
                        href = href.replace(/\/id\/([0-9]+)/, '/id/' + xhr.id);
                    }
                    $link
                            .attr('href', href)
                            .fadeIn();
                }

            } else {
                showModal('Fehler', xhr.message);
            }
        });
    });
    // Hide confirm box
    $('.suggest').hide();

    // Item delete confirmation
    $('section').on('click', '.delete-item', function(e) {
        e.preventDefault();
        var $elem = $(this),
                url = $(this).attr('href');

        if (confirm('Wirklich löschen?')) {
            $.getJSON(url, function(xhr) {
                if (xhr.success == true) {
                    $elem.closest('tr, li[id^="list_"], .help-block').fadeOut(function() {
                        $(this).remove();
                        $('input#invoice_pdf').remove();
                    });
                } else {
                    showModal('Fehler', xhr.error);
                }
            });
        }
    });

    // Inplace editor

    $("section").on('mouseenter', '.editable', function() {



        $(this).editInPlace({
            url: "/edit/rename-item/",
            value_required: true,
            // on_blur: null
        });
    });


    // Modal
    var $modal = '<div class="modal hide fade" id="modal">'
            + '  <div class="modal-header">'
            + '    <button type="button" class="close" data-dismiss="modal">×</button>'
            + '    <h3></h3>'
            + '  </div>'
            + '  <div class="modal-body">'
            + '  </div>'
            + '  <div class="modal-footer">'
            + '    <a href="#" class="btn btn-primary ok" data-dismiss="modal">Ok</a>'
            + '    <a href="#" class="btn" data-dismiss="modal">Abbrechen</a>'
            + '  </div>'
            + '</div>';

    $('body').append($modal);

    $('#modal').modal({
        show: false
    });

    $('#data-invaccnr').checkAccInput('select#entities-account');

    $('#accounts-form').newAccount();

    $('select[name="itemsperpage"]').change(function() {
        $(this).closest('form').submit();
    });

    if (window.location.hash) {
        var hash = window.location.hash;
        $('.nav-tabs a[href="' + hash + '"]').tab('show');
    }

    // nested set sortable
    $('ol.sortable').nestedSortable({
        disableNesting: 'no-nest',
        forcePlaceholderSize: true,
        handle: 'div',
        helper: 'clone',
        items: 'li',
        maxLevels: 0,
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        change: function(event, ui) {
            $('#save-ns').fadeIn();
        }
    });

    $('#toArray').click(function(e) {
        var arr = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 1});
        var jsonStr = JSON.stringify(arr);
        $.ajax({
            url: '/edit/save-tree/',
            type: 'POST',
            data: 'tree=' + jsonStr,
            dataType: 'JSON',
            success: function(xhr) {
                $('#ns-select').html(xhr.select);
                $('#save-ns').fadeOut();
            }
        });
    });

    $('a.combo-edit').hide();

    $('input.ui-autocomplete-input').keyup(function() {
        if ($(this).val() == '') {
            $(this).closest('.controls').find('a.combo-edit').fadeOut();
        }
    });
});


function isDate(txtDate)

{

    var currVal = txtDate;


console.log(currVal);
    if (currVal == '')
        return false;



    //Declare Regex 

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    
    var dtArray = currVal.match(rxDatePattern); // is format OK?

console.log(dtArray);

    if (dtArray == null)
        return false;

    dtDay = dtArray[1];

    dtMonth = dtArray[3];

    dtYear = dtArray[5];



    if (dtMonth < 1 || dtMonth > 12)
        return false;

    else if (dtDay < 1 || dtDay > 31)
        return false;

    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;

    else if (dtMonth == 2)

    {

        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;

    }

    return true;

}
